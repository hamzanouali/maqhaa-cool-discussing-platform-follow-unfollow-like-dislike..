<?php


//Clear Cache facade value:
Route::get('/clear-cache', function() {
    $exitCode = Artisan::call('cache:clear');
    return '<h1>Cache facade value cleared</h1>';
});

//Reoptimized class loader:
Route::get('/optimize', function() {
    $exitCode = Artisan::call('optimize');
    return '<h1>Reoptimized class loader</h1>';
});

//Route cache:
Route::get('/route-cache', function() {
    $exitCode = Artisan::call('route:cache');
    return '<h1>Routes cached</h1>';
});

//Clear Route cache:
Route::get('/route-clear', function() {
    $exitCode = Artisan::call('route:clear');
    return '<h1>Route cache cleared</h1>';
});

//Clear View cache:
Route::get('/view-clear', function() {
    $exitCode = Artisan::call('view:clear');
    return '<h1>View cache cleared</h1>';
});

//Clear Config cache:
Route::get('/config-cache', function() {
    $exitCode = Artisan::call('config:cache');
    return '<h1>Clear Config cleared</h1>';
});

Route::get('/sitemap.xml', 'SitemapController@index');

Route::group(['middleware'=>['auth']],function(){

  Route::get('/get-category-user/{id}',function($id){
    return \DB::table('user_category')
      ->where('category_id',$id)
      ->where('user_id',\Auth::id())
      ->get()
      ->count();
  })->where('id','[0-9]+');

  Route::get('/change-password',function(){
    return view('auth.passwords.changepassword');
  });

  Route::post('/update-info','ChangePasswordController@updateUserInfo');

  Route::get('/change-info',function(){
    return view('auth.changeinfo')->with(['name' => \Auth::user()->name , 'email' => \Auth::user()->email]);
  });

  Route::post('/changepassword','ChangePasswordController@update')->name('changepassword');

  // notification

  Route::get('/not-readed-notifications',function(){
    return \DB::table('notifications')
      ->where('notifiable_id',\Auth::id())
      ->where('read_at',null)
      ->take(10)
      ->get(['data','created_at','id','read_at']);
  });

  Route::get('/all-notifications/{skip}',function($skip = 0){
    return \DB::table('notifications')
    ->where('notifiable_id',\Auth::id())
    ->skip($skip)
    ->take(15)
    ->get();
  })->where('skip','[0-9]+');

  Route::post('/deleteAllNotifications',function(){
    \DB::table('notifications')
      ->where('notifiable_id',\Auth::id())
      ->delete();
  });

  Route::get('/count-not-readed-notifications',function(){
    return \DB::table('notifications')
      ->where('notifiable_id',\Auth::id())
      ->where('read_at',null)
      ->get()
      ->count();
  });

  Route::post('/markAsReadNotifications',function(){
    \DB::table('notifications')
      ->where('notifiable_id',\Auth::id())
      ->update(['read_at'=>\Carbon\Carbon::now()]);
  });

  /*-------------
  START : LIKE DISKLIKE ROUTES
  -------------*/
  Route::post('/like-dislike','LikeController@create');

  Route::post('/save-user-info','SkillController@updateUserInfo');

  Route::post('/block-user/{id}',function($id){
    \DB::table('users')->where('id',$id)->update(['is_active'=>2]);
    return 'true';
  })->where('id','[0-9]+')->middleware('role:1');

  Route::post('/unblock-user/{id}',function($id){
      \DB::table('users')->where('id',$id)->update(['is_active'=>1]);
      return 'true';
    })->where('id','[0-9]+')->middleware('role:1');

  Route::post('/make-a-writer/{id}',function($id){
    \DB::table('users')->where('id',$id)->update(['role'=>2]);
    return 'true';
  })->where('id','[0-9]+')->middleware('role:1');

  Route::post('/make-a-user/{id}',function($id){
      \DB::table('users')->where('id',$id)->update(['role'=>0]);
      return 'true';
    })->where('id','[0-9]+')->middleware('role:1');

  Route::post('/post_a_publication','publicationController@create');

  Route::post('/update_a_publication/{id}','publicationController@update')->where('id','[0-9]+');

  Route::post('/update_a_comment/{id}','CommentController@update')->where('id','[0-9]+');

  Route::post('/post_a_comment','CommentController@create');

  Route::post('save_tag','TagsController@create')->middleware('role:1');

  Route::post('removeTag','TagsController@destroy')->middleware('role:1');

  Route::post('save_category','CategoriesController@create')->middleware('role:1');

  Route::post('/edit-category','CategoriesController@edit')->middleware('role:1');

  Route::post('/save_followed_categories','CategoriesController@saveForUser');

  Route::post('/save_followed_category','CategoriesController@saveOneForUser');

  Route::post('/unfollow_category','CategoriesController@unfollow');

  Route::post('removecategory','CategoriesController@destroy')->middleware('role:1');

  Route::get('/getRememberList',function(){
      $data = \DB::table('remember_list')->where('user_id','=',\Auth::id())->get(['id','title']);
      return json_encode($data);
  });

  Route::post('/addToRememberList','RememberListController@create');

  Route::get('/deleteRememberList',function(){
      $data = \DB::table('remember_list')->where('user_id','=',\Auth::id())->delete();
      return json_encode($data);
  });

  Route::post('/save-skill','SkillController@create');

  // website routes
  Route::post('/update-instructions-of-use','WebsiteController@updateInstructions')->middleware('role:1');

  Route::post('/update-posting-rules','WebsiteController@updatePostingRules')->middleware('role:1');

  // Get some informations for dashboard
  // ( number of users, publications, comments and likes )
  Route::get('/get-website-info',function(){
    $coll = collect();
    $coll = $coll->concat(collect(\DB::table('users')->get()->count()));
    $coll = $coll->concat(collect(\DB::table('publications')->get()->count()));
    $coll = $coll->concat(collect(\DB::table('comments')->get()->count()));
    $coll = $coll->concat(collect(\DB::table('likes')->get()->count()));
    return json_encode($coll);
  })->middleware('role:1');

  Route::post('/switch_user_role/{id}/{role}',function($id,$role){
    \DB::table('users')->where('id',$id)->update([
      'role' => $role
    ]);
  })->where(['id'=>'[0-9]+','role'=>'[0-1]'])->middleware('role:1');

  Route::post('/close-target','publicationController@closeTarget');

  Route::post('/open-target','publicationController@openTarget');

  Route::post('/remove_publication/{id}',function($id){
    $obj = \App\Publication::with('user')->where('id','=',$id)->first();
    if(
      $obj->user->id != \Auth::id() &&
      \Auth::user()['role'] != 1
    ){
      return false;
    }

    if($obj->comments != 0 && $obj->likes != 0 && \Auth::user()['role'] != 1) {
      return '401-forbidden';
    }

    \DB::table('publications')->where('id',$id)->delete();
    \DB::table('comments')->where('publication_id',$id)->delete();
    \DB::table('likes')->where('target_type','publication')->where('target_id',$id)->delete();

    return 'true';
  })->where('id','[0-9]+');

  // delete comment
  Route::post('/remove_comment/{id}',function($id){

    // if the comment has replies
    // I delete just the comment because i can't remove all the tree

    // full comment
    $comment = \App\Comment::with('user')->where('id',$id)->first();

    if(\Auth::user()->role != 1 && \Auth::id() != $comment->user->id) return '401-forbidden';

    if($comment->replies > 0){
      if(\Auth::user()->role != 1) return '401-forbidden';

      \DB::table('publications')->where('id',$comment->publication_id)->update([
        'comments' => \DB::raw(' comments -'.$comment->replies)
      ]);
    }

    \DB::table('comments')->where('id',$id)->delete();
    if($comment->parent_comment_id != 0) \DB::table('comments')->where('id',$comment->parent_comment_id)->decrement('replies');
    \DB::table('likes')->where('target_type','comment')->where('target_id',$id)->delete();
    \DB::table('publications')->where('id',$comment->publication_id)->decrement('comments');

    return 'true';

  })->where('id','[0-9]+');

});

Route::get('/get-instructions-of-use',function(){
  return json_encode(\DB::table('website')->first(['instructions_of_use']));
});

Route::get('/get-posting-rules',function(){
  return json_encode(\DB::table('website')->first(['posting_rules']));
});
//
Route::get('/get_publications',function(){
  $q = \App\Publication::with('tag')->get();
  echo "<pre>";
  // $q = $publication->with('tag')->get();
  foreach ($q as $v1) {

    echo "<b>title:<b> ".$v1->title."\n";
    echo "tags: ";
    foreach ($v1->tag as $value) {
      print_r($value->name.", ");
    }
    echo "<hr>";

  }
  echo "</pre>";
});

/*=================[[ SEARCH FOR publicationS ]]=================== */

//search for publications
Route::get('/search/{title}/{skip}',function($title,$skip){

  // empty string
  if(empty($title) || $title == ' ') return '';

  // convert title to lower case
  // $title = strtolower($title);

  // number of words
  $num_of_words = count(explode(" ",$title));

  // last position of white space
  $last_white_space = strrpos($title," ");

  if($last_white_space == 0 && !empty($title)) {
    return \App\Publication::where('title','LIKE','%'.$title.'%')
    ->with('user')
    ->with('category')
    ->skip($skip)
    ->take(15)
    ->get();
  }else if($last_white_space == 0){
    return json_encode([]);
  }


  // loop through title words
  while($last_white_space != 0) {
    $title = substr($title,0,$last_white_space);
    // search for publications by title
    $res = \App\Publication::where('title','LIKE','%'.$title.'%')
    ->with('user')
    ->with('category')
    ->skip($skip)
    ->take(15)
    ->get();
    if($res->count() > 0){
      return $res;
    }
    $last_white_space = strrpos($title," ");
  }

})->where(['title' => '[a-z\p{Arabic}\sA-Z0-9]+','skip' => '[0-9]+']);

//search *** BY CATEGORY ***
Route::get('/search-by-category/{title}/{skip}/{id}',function($title,$skip,$id){

  // empty string
  if(empty($title) || $title == ' ') return '';

  // convert title to lower case
  // $title = strtolower($title);

  // number of words
  $num_of_words = count(explode(" ",$title));

  // last position of white space
  $last_white_space = strrpos($title," ");

  if($last_white_space == 0 && !empty($title)) {
    return \App\Publication::where('title','LIKE','%'.$title.'%')
    ->with('user')
    ->with('category')
    ->where('category_id',$id)
    ->skip($skip)
    ->take(15)
    ->get();
  }else if($last_white_space == 0){
    return json_encode([]);
  }
  //

  // loop through title words
  while($last_white_space != 0) {
    $title = substr($title,0,$last_white_space);
    // search for publications by title
    // then push the result to the collection
    $res = \App\Publication::where('title','LIKE','%'.$title.'%')
    ->with('user')
    ->with('category')
    ->where('category_id',$id)
    ->skip($skip)
    ->take(15)
    ->get();
    if($res->count() > 0){
      return $res;
    }
    $last_white_space = strrpos($title," ");
  }

})->where(['title' => '[a-z\p{Arabic}\sA-Z0-9]+','skip' => '[0-9]+','id' => '[0-9]+']);

//search *** BY USER ***
Route::get('/search-by-user/{title}/{skip}/{id}',function($title,$skip,$id){

  // empty string
  if(empty($title) || $title == ' ') return '';

  // convert title to lower case
  // $title = strtolower($title);

  // number of words
  $num_of_words = count(explode(" ",$title));

  // last position of white space
  $last_white_space = strrpos($title," ");

  if($last_white_space == 0 && !empty($title)) {
    return \App\Publication::where('title','LIKE','%'.$title.'%')
    ->with('user')
    ->with('category')
    ->where('user_id',$id)
    ->skip($skip)
    ->take(15)
    ->get();
  }else if($last_white_space == 0){
    return json_encode([]);
  }
  //

  // loop through title words
  while($last_white_space != 0) {
    $title = substr($title,0,$last_white_space);
    // search for publications by title
    // then push the result to the collection
    $res = \App\Publication::where('title','LIKE','%'.$title.'%')
    ->with('user')
    ->with('category')
    ->where('user_id',$id)
    ->skip($skip)
    ->take(15)
    ->get();
    if($res->count() > 0){
      return $res;
    }
    $last_white_space = strrpos($title," ");
  }

})->where(['title' => '[a-z\p{Arabic}\sA-Z0-9]+','skip' => '[0-9]+','id' => '[0-9]+']);

/*=================[[ END SEARCH FOR publicationS ]]=================== */

//search for users
Route::get('/search_for_users/{name}/{skip}/{take}',function($name,$skip,$take){

  // empty string
  if(empty($name) || $name == ' ') return '';

  // convert title to lower case
  // $name = strtolower($name);

  // number of words
  $num_of_words = count(explode(" ",$name));

  // last position of white space
  $last_white_space = strrpos($name," ");

  if($last_white_space == 0 && !empty($name)) {
    return \App\User::where('name','LIKE','%'.$name.'%')->skip($skip)->take($take)->get();
  }else if($last_white_space == 0){
    return json_encode([]);
  }
  //
  // print_r(substr($title,0,$number_of_words));

  // loop through title words
  while($last_white_space != 0) {
    $name = substr($name,0,$last_white_space);
    // search for publications by title
    // then push the result to the collection
    $res = \App\User::where('name','LIKE','%'.$name.'%')->skip($skip)->take($take)->get();
    if($res->count() > 0){
      return $res;
    }
    $last_white_space = strrpos($name," ");
  }

})->where(['name' => '[a-z\p{Arabic}\sA-Z0-9]+','skip' => '[0-9]+','take' => '[0-9]+']);

Route::get('/get-users/{skip}/{order?}',function($skip,$order = null){
  return \DB::table('users')
    ->latest()
    ->skip($skip)
    ->take(15)
    ->get();
})->where('skip','[0-9]+');


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


   /*|
    |
    |
    |
    |
    |
    |
    |
    |
    -------------
    START : AUTH ROUTES
    -------------*/

  Auth::routes();

  Route::get('/isAuth',function(){
      //return \Auth::user()->role.' ?= '.\Auth::user()['role'];
      $obj;
      if(\Auth::check()){
        $user = \DB::table('user_info')->where('user_id',\Auth::user()['id'])->first();
        $obj = '{'.
        ' "id" : '.\Auth::user()['id'].','.
        ' "name" : "'.\Auth::user()['name'].'",'.
        ' "is_active" : "'.\Auth::user()['is_active'].'",'.
        ' "role" : "'.\Auth::user()['role'].'",'.
        ' "created_at" : "'.\Auth::user()['created_at'].'",'.
        ' "profile_img" : "'.\Auth::user()['profile_img'].'",'.
        ' "points" : '.$user->likes.','.
        ' "comments" : '.$user->comments.','.
        ' "publications" : '.$user->publications.','.
        ' "reputation" : '.($user->comments*0.25 + $user->publications * 0.5 + $user->likes).''.
        '}';
      }else{
        $obj = 'false';
      }
      return $obj;
  });

  Route::get('/logout',function(){
    \Auth::logout();
    return redirect('/login');
    // return \Auth::check() ? 'true' : 'false';
  });

  Route::get('/ger-user-info/{id}',function($id){
    $collection = collect();
    $user = \DB::table('users')->where('id',$id)->first(['name','profile_img','created_at']);
    $coll1 = collect(json_encode(['name' => $user->name, 'profile_img' => $user->profile_img, 'created_at' => $user->created_at]));
    $collection = $collection->concat($coll1);
    $collection = $collection->concat(\DB::table('user_info')->where('user_id',$id)->get());
    return $collection;
  })->where('id','[0-9]+');

/*|
  |
  |
  |
  |
  |
  |
  |
  |
  -------------
  START : SKILL ROUTES
  -------------*/
  Route::get('/get-skills/{id}/{skip}','SkillController@index')
    ->where(['id'=>'[0-9]+','skip'=>'[0-9]+']);

/*|
  |
  |
  |
  |
  |
  |
  |
  |
  -------------
  START : publicationS ROUTES
  -------------*/


  Route::get('/get_all_publications/{skip}',function($skip){

    if(\Auth::check()){
      return \App\Publication::whereRaw('
        category_id = ANY (
          SELECT category_id FROM
          user_category WHERE
          user_id = '.\Auth::id().'
        )
      ')
      ->take(15)
      ->skip($skip)
      ->latest()
      ->with('user')
      ->with('category')
      ->get();
    }else {
      return \App\Publication::with('user')->with('category')->latest()->take(15)->skip($skip)->get();
    }

  })->where('skip','[0-9]+');

  // get comments for dashboard
  Route::get('/get-all-comments/{skip}',function($skip){

    return \App\Comment::with('user')->with('publication')->latest()->take(30)->skip($skip)->get();

  })->where('skip','[0-9]+')->middleware('role:1');

  Route::get('/get_all_publications_by_category/{skip}/{category_id}',function($skip,$category_id){

    return \App\Publication::with('user')->with('category')->latest()->where('category_id',$category_id)->take(15)->skip($skip)->get();

  })->where(['category_id'=>'[0-9]+','skip'=>'[0-9]+']);

  Route::get('/get_all_publications_by_user/{skip}/{user_id}',function($skip,$user_id){

    return \App\Publication::with('user')->with('category')->latest()->where('user_id',$user_id)->take(15)->skip($skip)->get();

  })->where(['user_id'=>'[0-9]+','skip'=>'[0-9]+']);

  // Get common publications
  Route::get('/get-common-publications/{skip}',function($skip){
    // for auth user
    if(\Auth::check()){
      return \App\Publication::whereRaw('
        category_id = ANY (
          SELECT category_id FROM
          user_category WHERE
          user_id = '.\Auth::id().'
        )
      ')
      ->with('user')
      ->with('category')
      ->where('created_at','>',date("Y-m-d", strtotime( '-7 days' ) ))
      ->orderBy('comments','desc')
      ->skip($skip)
      ->take(15)
      ->get();

    // for guest
    }else{
      return \App\Publication::with('user')
      ->with('category')
      ->where('created_at','>',date("Y-m-d", strtotime( '-7 days' ) ))
      ->orderBy('comments','desc')
      ->skip($skip)
      ->take(15)
      ->get();
    }
  })->where(['skip' => '[0-9]+','take' => '[0-9]+']);

  // Get common publications for certain category
  Route::get('/get-common-publications-for-this-category/{skip}/{id}',function($skip,$id){

    return \App\Publication::with('user')
    ->with('category')
    ->where('created_at','>',date("Y-m-d", strtotime( '-7 days' ) ))
    ->where('category_id',$id)
    ->orderBy('comments','desc')
    ->skip($skip)
    ->take(15)
    ->get();

  })->where(['skip' => '[0-9]+','id' => '[0-9]+']);

  // Get common publications for certain user
  Route::get('/get-common-publications-for-this-user/{skip}/{id}',function($skip,$id){

    return \App\Publication::with('user')
    ->with('category')
    ->where('created_at','>',date("Y-m-d", strtotime( '-7 days' ) ))
    ->where('user_id',$id)
    ->orderBy('comments','desc')
    ->skip($skip)
    ->take(15)
    ->get();

  })->where(['skip' => '[0-9]+','id' => '[0-9]+']);

  // Get best of  *** DAY *** publications for certain category
  Route::get('/get-best-of-day-publications-by-category/{skip}/{id}',function($skip,$id){

    return \App\Publication::with('user')
    ->with('category')
    ->whereDate('created_at', \Carbon\Carbon::today())
    ->where('category_id',$id)
    ->orderBy('likes','desc')
    ->skip($skip)
    ->take(15)
    ->get();

  })->where(['skip' => '[0-9]+','id' => '[0-9]+']);

  // Get best of  *** DAY *** publications for certain user
  Route::get('/get-best-of-day-publications-for-this-user/{skip}/{id}',function($skip,$id){

    return \App\Publication::with('user')
    ->with('category')
    ->whereDate('created_at', \Carbon\Carbon::today())
    ->where('user_id',$id)
    ->orderBy('likes','desc')
    ->skip($skip)
    ->take(15)
    ->get();

  })->where(['skip' => '[0-9]+','id' => '[0-9]+']);

  // Get best of *** DAY *** publications
  Route::get('/get-best-of-day-publications/{skip}',function($skip){
    // for auth user
    if(\Auth::check()){
      return \App\Publication::whereRaw('
        category_id = ANY (
          SELECT category_id FROM
          user_category WHERE
          user_id = '.\Auth::id().'
        )
      ')
      ->with('user')
      ->with('category')
      ->whereDate('created_at', \Carbon\Carbon::today())
      ->orderBy('likes','desc')
      ->skip($skip)
      ->take(15)
      ->get();
    }
    // guest user
    else {
      return \App\Publication::with('user')
      ->with('category')
      ->whereDate('created_at', \Carbon\Carbon::today())
      ->orderBy('likes','desc')
      ->skip($skip)
      ->take(15)
      ->get();
    }
  })->where(['skip' => '[0-9]+','take' => '[0-9]+']);

  // Get best of *** ALL *** publications for certain category
  Route::get('/get-best-of-all-publications-by-category/{skip}/{id}',function($skip,$id){

    return \App\Publication::with('user')
    ->with('category')
    ->where('category_id',$id)
    ->orderBy('likes','desc')
    ->skip($skip)
    ->take(15)
    ->get();

  })->where(['skip' => '[0-9]+','id' => '[0-9]+']);

  // Get best of *** ALL *** publications for certain user
  Route::get('/get-best-of-all-publications-for-this-user/{skip}/{id}',function($skip,$id){

    return \App\Publication::with('user')
    ->with('category')
    ->where('user_id',$id)
    ->orderBy('likes','desc')
    ->skip($skip)
    ->take(15)
    ->get();

  })->where(['skip' => '[0-9]+','id' => '[0-9]+']);

  // Get best of *** ALL *** publications
  Route::get('/get-best-of-all-publications/{skip}',function($skip){
    // for auth user
    if(\Auth::check()){
      return \App\Publication::whereRaw('
        category_id = ANY (
          SELECT category_id FROM
          user_category WHERE
          user_id = '.\Auth::id().'
        )
      ')
      ->with('user')
      ->with('category')
      ->orderBy('likes','desc')
      ->skip($skip)
      ->take(15)
      ->get();
    }
    // guest user
    else {
      return \App\Publication::with('user')
      ->with('category')
      ->orderBy('likes','desc')
      ->skip($skip)
      ->take(15)
      ->get();
    }
  })->where(['skip' => '[0-9]+','take' => '[0-9]+']);

  Route::get('/get_latest_commented_posts/{user_id}',function($user_id){
    return \App\Comment::latest()->where('user_id',$user_id)->with('publication')->take(10)->get();
  })->where(['user_id' => '[0-9]+']);

  Route::get('publications/{id}','publicationController@show')->where('id','[0-9]+');

  Route::get('/get_best_of_week_publications/{skip}/{take}',function($skip,$take){
    return \DB::table('publications')->where('created_at','>',date("Y-m-d", strtotime( '-7 days' ) ))->orderBy('likes','desc')->take(10)->get(['id','title','comments']);
  })->where(['skip' => '[0-9]+','take' => '[0-9]+']);

  Route::get('/get_publications_in_the_same_category/{category_id}',function($category_id){
    return \DB::table('publications')->where('category_id',$category_id)->latest()->take(10)->get(['id','title','comments']);
  })->where('category_id','[0-9]+');

  Route::get('/get_publications_by_random',function(){
    return \DB::table('publications')
    ->inRandomOrder()
    ->take(5)
    ->get(['id','title','comments']);
  });

/*|
  |
  |
  |
  |
  |
  |
  |
  |
  -------------
  START : COMMENTS PAGE ROUTES
  -------------*/

  Route::get('comments/{publication_id}',function($publication_id){
    return \App\Comment::where('publication_id','=',$publication_id)->where('parent_comment_id',0)->with('user')->get();
  })->where('publication_id','[0-9]+');
  // replies are here too
  Route::get('/comments/{parent_comment_id}/{publication_id}',function($parent_comment_id,$publication_id){
    return \App\Comment::latest()
      ->where('parent_comment_id',$parent_comment_id)
      ->where('publication_id',$publication_id)
      ->with('user')
      ->get();
  })->where(['parent_comment_id' => '[0-9]+', 'publication_id' => '[0-9]+']);


/*|
  |
  |
  |
  |
  |
  |
  |
  |
  -------------
  START : CATEGORIES ROUTES
  -------------*/

  Route::get('/get_old_categories',function(){

    if(!\Auth::check()) return \DB::table('categories')->get(['id','category']);

    return \App\Category::whereRaw('
      id = ANY (
        SELECT category_id FROM
        user_category WHERE
        user_id = '.\Auth::id().'
      )
    ')
    ->get();
  });

  Route::get('/get_categories',function(){
    return \DB::table('categories')->get(['id','category']);
  });

  Route::get('/activate-account/{token}',function($token){
    if(\DB::table('users')->where('email',Crypt::decryptString($token))->get()->count() > 0){
      \DB::table('users')->where('email',Crypt::decryptString($token))->update(['is_active'=>1]);
    }else{
      // redirect to random route,then will be redirected to 404 page by laravel automatically
      return redirect('/404-page');
    }
    return redirect('/categories/follow')->with('status', __('messages.account_has_been_activated'));
  });


  Route::get('/contact',function(){
    return view('contact');
  });

  Route::post('/contact/send','ContactController@send');

  Route::get('/search',function(){
    return view('search');
  });

  Route::get('/alert',function(){
    return view('errors.alert');
  });

  /**///// [ INSTALLER ROUTES ] ////// **/

  /*
  =============
  HOME PAGE FOR THE INSTALLER
  =============
  */

  Route::get('/installer',function(){
    return view('installer.home');
  });

  /*
  =============
  THEME PAGE
  =============
  */

  Route::get('/installer/theme',function(){
    return view('installer.theme');
  });

  /*
  =============
  SET LANGUAGE
  =============
  */

  Route::get('/set-language/{language}',function($language){

    $folders = scandir(base_path().'/resources/lang');
    // result: [ 0 => '.', 1 => '..', 1 => 'en' ]
    // so we remove the two first values
    $folders = array_slice($folders,2);
    // resutlt [ 0 => 'en' ]

    // if language exists in /resources/lang directiory
    if(in_array($language,$folders)){
      
      // set the language to a cookie
      if(isset($_COOKIE['language'])){
        setcookie(
          'language',
          $language,
          time() + (10 * 365 * 24 * 60 * 60),
          '/'
        );
      }else{
        setcookie(
          'language',
          $language,
          time() + (10 * 365 * 24 * 60 * 60)
        );
      }
  
      app()->setLocale($language);
      return redirect('/'.$language);
    }
    
    return redirect('/404-page');

  })->where(['language'=>'[a-z]+']);

  /*
  =============
  PAGE TO SHOW THE LANGUAGES THAT EXISTS
  =============
  */

  Route::get('/installer/language',function(){

    $folders = scandir(base_path().'/resources/lang');
    // result: [ 0 => '.', 1 => '..', 1 => 'en' ]
    // so we remove the two first values
    $folders = array_slice($folders,2);
    // resutlt [ 0 => 'en' ]

    return view('installer.languages')->with(['languages' => $folders]);
  });

  /*
  =============
  PAGE TO SHOW THE FILES THAT EXISTS IN EACH LANGUAGE
  =============
  */

  Route::get('/installer/language/{language}',function($language){

    $files = scandir(base_path().'/resources/lang/'.$language);
    // result: [ 0 => '.', 1 => '..', 1 => 'file_name' ]
    // so we remove the two first values
    $files = array_slice($files,2);
    // resutlt [ 0 => 'file_name' ]

    return view('installer.files')->with(['language'=> $language,'files' => $files]);

  })->where('language','[a-z]+');

  /*
  =============
  PAGE TO SHOW THE DICTIONARY OF EACH FILE
  =============
  */

  Route::get('/installer/language/{language}/{page}',function($language,$page){

    app()->setlocale($language);

    return view('installer.language')
    ->with([
      'language' => $language,
      'page'=>$page,
      'dictionary' => __($page)
      ]);
    
  })->where(['language'=>'[a-z]+','page'=>'[a-z]+']);


  /*
  =============
  CHANGE COLORS PATH
  =============
  */

  Route::get('/installer/theme/color',function(){

    return view('installer.color')->with([
      'colors' => [
        '#966363'=>'#E57373',
        '#733f1d'=>'#fb9551',
        '#b37764'=>'#FFAB91',
        '#71214c'=>'#EC407A',
        '#71214c'=>'#fd8fb4',
        '#636b1e'=>'#CDDC39',
        '#333e7b'=>'#b1bcf8',
        '#5b7a87'=>'#a7c0cb',
        '#54817d'=>'#90dcd4',
        '#878787'=>'#FFF59D',
        '#777777'=>'#ffeb3a',
        '#7a6f90'=>'#d7c2ff',
        '#818e73'=>'#DCEDC8',
        
      ]
    ]);
    
  })->where(['language'=>'[a-z]+','page'=>'[a-z]+']);

  /*
  =============
  CREATE A NEW LANGUAGE
  =============
  */

  Route::post('/create-new-language','WebsiteController@newLanguage');

  /*
  =============
  SET THE COLOR OF THE THEME
  =============
  */

  Route::get('/set-color/{index}/{content}',function($index,$content){

    // get css file  
    $app_css_file = file_get_contents(base_path().'/public/css/app.css');
    // get json file contains the current colors
    $current_colors = json_decode(file_get_contents(base_path().'/resources/theme/colors.json'));
    // update colors for css file
    $app_css_file = str_replace(
      $current_colors->primary,
      '#'.$content,
      $app_css_file
    );
    $app_css_file = str_replace(
      $current_colors->font_primary,
      '#'.$index,
      $app_css_file
    );

    // save the changes in css file
    file_put_contents(base_path().'/public/css/app.css',$app_css_file);

    file_put_contents(
      base_path().'/resources/theme/colors.json',
      ' {
          "primary" : "#'.$content.'",
          "font_primary" : "#'.$index.'"
        } 
      '
    );

    return redirect('/');

  });


  /*
  =============
  DELETE A LANGUAGE
  =============
  */

  // it's just removing directories, i am lazy to work with post method  
  Route::get('/delete-language/{language}',function($language){

    $dirPath = base_path().'/resources/lang/'.$language.'';

    if (! is_dir($dirPath)) {
        throw new InvalidArgumentException("$dirPath must be a directory");
    }
    if (substr($dirPath, strlen($dirPath) - 1, 1) != '/') {
        $dirPath .= '/';
    }
    $files = glob($dirPath . '*', GLOB_MARK);
    foreach ($files as $file) {
        if (is_dir($file)) {
            self::deleteDir($file);
        } else {
            unlink($file);
        }
    }
    rmdir($dirPath);

    return redirect()->back();

  })->where(['language'=>'[a-z]+']);

  /*
  =============
  UPDATE LANGUAGE
  =============
  */

  Route::post('/installer/language/{language}/{page}/update','WebsiteController@updateLanguage')
  ->where(['language'=>'[a-z]+','page'=>'[a-z]+']);


  Route::get('/dashboard/{vue_capture?}', function () {
    return view('home');
  })->where('vue_capture', '(.*)')->middleware('auth','role:1');

  Route::get('/{vue_capture?}', function () {
    return view('welcome');
  })->where('vue_capture', '(.*)');









