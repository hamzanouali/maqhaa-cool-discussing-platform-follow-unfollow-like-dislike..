<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <meta name="description" content=" ">
        <meta name="keywords" content="">
        <meta name="author" content="">

        <link rel="icon" sizes="24x24" type="image/png" href="/images/icon.png">
        
        <?php if(isset(explode('/',$_SERVER['REQUEST_URI'])[4])) { ?>
          <title> {!! explode('/',$_SERVER['REQUEST_URI'])[4] !!} </title>
        <?php } else { ?>
          <title> {!! config('app.name') !!} </title>
        <?php } ?>
        
        <link rel="stylesheet" href="/css/app.css">
    </head>
    <body class="">

      <!-- Vuejs app container -->
      @if(
        app()->getLocale() == 'ar' ||
        app()->getLocale() == 'he' ||
        app()->getLocale() == 'pe'
      )
      <div id="app" class="women_street_style">
      @else 
      <div id="app" class="women_street_style ltr_style">
      @endif
        <router-view></router-view>
      </div>    
           
      <script>   

        /*** [ SET DEFAULT LANGUAGE ] ***/

        window.default_laguage = 'en';

        /*** [ GOOGLE ANALYTICS APP KEY ] ***/

        window.google_analytics_app_key = 'AA-XXXXXXXXX-X';

        /*** [ TEXT EDITOR CONFIG ] ***/

        /*
        EDITOR: Quill.js
        -----------------
        if you want to prevent somthing, example:

        if you want to prevent posting images:
        - remove it from the quill_toolbar
        - remove it from quill_formats
        */

        window.quill_tollbar = [
            ['bold', 'italic', 'underline'],
            ['blockquote', 'code-block'],
            [{ 'list': 'ordered'}, { 'list': 'bullet' }],
            [{ 'direction': 'rtl' }],                
            [{ header: [1, 2, false] }],
            [{ 'align': [] }],
            ['link']
        ];

        window.quill_formats = [
          'background',
          'bold',
          'color',
          'font',
          'code',
          'italic',
          'link',
          'size',
          'strike',
          'script',
          'underline',
          'blockquote',
          'header',
          'indent',
          'list',
          'align',
          'direction',
          'code-block',
          'formula'
          // 'image',
          // 'video',
        ];


        /*** [ DEVELOPERS AREA ] ***/
        /*

          WARNING: if you can't understand the code below, don't touche it please.
          Email: nouali.hamza.1998@gmail.com
          Feel free to email me.

        */

        // get all folders in /resources/lang
        let folders_in_lang = <?php echo json_encode(array_slice(scandir(base_path().'/resources/lang'),2)) ?>; 
        // init dictionary
        window.dictionary = {};
        // push available languages to dictionary  
        folders_in_lang.forEach(val => { 
          window.dictionary[val] = {}
        });
        // push the dictionary of the locale language 
        window.dictionary['<?php echo app()->getLocale() ?>'] = <?php echo json_encode(__('dashboard_vue')) ?>;

      </script>

      <script src="/js/manifest.js"></script>
      <script src="/js/vendor.js"></script>
      <script src="/js/app.home.js"></script>

    </body>
</html>
