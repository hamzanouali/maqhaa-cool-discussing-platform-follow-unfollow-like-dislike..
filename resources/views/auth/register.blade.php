@extends('layouts.app')

@section('content')
<br><br><br><br>
  <div class="container-fluid"id="login">

    <div>
      <a href="/">
        <h3> {{ __('messages.back_to_home_page') }} </h3>
      </a>
      <br />
      <br />
    </div>


    <div class="columns">

        <div class="column is-4">
        </div>

        <div class="column loginDiv" >

        <form class="form-horizontal text-center" method="POST" action="{{ route('register') }}">
            {{ csrf_field() }}
          <!-- <p class="is-centered"><img src="/images/icons8_Space_Shuttle_100px.png" alt="إفعلها الآن"></p> -->
          <h1>  {{ __('messages.create_account') }} </h1>

          <div>

            <div class="label">
              {{ __('messages.username') }}
            </div>
            <div class="field form-group{{ $errors->has('name') ? ' has-error' : '' }}">
              <div class="control">
                <input id="name" type="text" placeholder="{{ __('messages.username') }}" class="form-control" name="name" value="{{ old('name') }}" required autofocus>
                @if ($errors->has('name'))
                    <div class="help-block">
                        <strong>{{ $errors->first('name') }}</strong>
                    </div>
                @endif
              </div>
            </div>

            <div class="label">
              {{ __('messages.email') }}
            </div>
            <div class="field form-group{{ $errors->has('email') ? ' has-error' : '' }}">
              <div class="control">
                <input id="email" type="email" placeholder="{{ __('messages.email') }}" class="form-control" name="email" value="{{ old('email') }}" required autofocus>
                @if ($errors->has('email'))
                    <div class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </div>
                @endif
              </div>
            </div>

            <div class="label">
              {{ __('messages.password') }}
            </div>
            <div class="field form-group{{ $errors->has('password') ? ' has-error' : '' }}">
              <div class="control">
                <input id="password" type="password" placeholder="{{ __('messages.password') }}" class="form-control" name="password" required>
                @if ($errors->has('password'))
                    <div class="help-block">
                        <strong>{{ $errors->first('password') }}</strong>
                    </div>
                @endif
              </div>
            </div>

            <div class="label">
              {{ __('messages.confirm_password') }}
            </div>
            <div class="field form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
              <div class="control">
                <input id="password_confirmation" type="password" placeholder="{{ __('messages.confirm_password') }}" class="form-control" name="password_confirmation" required>
                @if ($errors->has('password_confirmation'))
                    <div class="help-block">
                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                    </div>
                @endif
              </div>
            </div>

            <div class="field button-field">
                <button class="myloginBTN button is-primary is-large">
                  {{ __('messages.login') }}
                </button>
            </div>
            <!-- forgot password -->
            <p><a href="{{ route('password.request') }}"> {{ __('messages.forgot_the_password') }} </a></p>
            <p><a href="{{ route('logout') }}"> {{ __('messages.login') }} </a></p>

          </div>
        </form>
        </div>

        <div class="column is-4">
        </div>
    </div>
  </div>

@endsection
