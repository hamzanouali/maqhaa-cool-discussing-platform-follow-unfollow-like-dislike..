@extends('layouts.app')

@section('content')

<br><br><br><br><br><br>
  <div class="container-fluid"id="login">
    <div>
      <a href="/">
        <h3> {{ __('messages.back_to_home_page') }} </h3>
      </a>
      <br />
      <br />
    </div>
    <div class="columns">

        <div class="column is-4">
        </div>

        <div class="column loginDiv" >

        <form class="form-horizontal text-center" method="POST" action="{{ route('changepassword') }}">
            {{ csrf_field() }}
          <h1> {{ __('messages.change_password') }} </h1>

            @if(session()->has('message'))
                <b-message type="is-warning" class="nextCaseWarning" dir="rtl">
                    {{ session()->get('message') }}
                </b-message>
            @endif

          <div>

            <div class="label">
              {{ __('messages.current_password') }}
            </div>
            <div class="field form-group{{ $errors->has('password') ? ' has-error' : '' }}">
              <div class="control">
                <input id="password" type="password" placeholder="{{ __('messages.current_password') }}" class="form-control" name="password" required>
                @if ($errors->has('password'))
                    <div class="help-block">
                        <strong>{{ $errors->first('password') }}</strong>
                    </div>
                @endif
              </div>
            </div>

            <div class="label">
              {{ __('messages.new_password') }}
            </div>
            <div class="field form-group{{ $errors->has('newpassword') ? ' has-error' : '' }}">
              <div class="control">
                <input id="password" type="password" placeholder=" {{ __('messages.new_password') }} " class="form-control" name="newpassword" required>
                @if ($errors->has('new_password'))
                    <div class="help-block">
                        <strong>{{ $errors->first('newpassword') }}</strong>
                    </div>
                @endif
              </div>
            </div>

            <div class="label">
              {{ __('messages.confirm_password') }}
            </div>
            <div class="field form-group{{ $errors->has('newpassword_confirmation') ? ' has-error' : '' }}">
              <div class="control">
                <input id="password" type="password" placeholder=" {{ __('messages.confirm_password') }} " class="form-control" name="newpassword_confirmation" required>
                @if ($errors->has('newpassword_confirmation'))
                    <div class="help-block">
                        <strong>{{ $errors->first('newpassword_confirmation') }}</strong>
                    </div>
                @endif
              </div>
            </div>

            <div class="field button-field">
                <button class="myloginBTN button is-primary is-large">
                  {{ __('messages.update') }}
                </button>
            </div>
            <!-- forgot password -->
            <p><a href="/change-info"> {{ __('messages.edit_personal_info') }} </a></p>
            <div class="help-block" dir="rtl">
                <strong> {{ __('messages.note') }} </strong><br/><br/>
                <strong> {{ __('messages.change_password_instructions') }} </strong>
            </div>

          </div>

        </form>
        </div>

        <div class="column is-4">
        </div>
    </div>
  </div>
@endsection
