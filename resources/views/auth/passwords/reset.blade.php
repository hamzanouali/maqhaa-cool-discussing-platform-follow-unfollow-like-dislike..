@extends('layouts.app')

@section('content')
<div class="container-fluid" id="login">
<br><br><br>
  <div>
    <a href="/">
      <h3> {{ __('messages.back_to_home_page') }} </h3>
    </a>
    <br />
    <br />
  </div>

  <div class="columns">

      <div class="column is-4">
      </div>

      <div class="column loginDiv" >

      <form method="POST" action="{{ route('password.request') }}">
          {{ csrf_field() }}

        <h1> {{ __('messages.recover_password') }} </h1>

        @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @endif

        <div>

          <div class="label">
            {{ __('messages.email') }}
          </div>
          <div class="field {{ $errors->has('email') ? ' has-error' : '' }}">
            <div class="control">
              <input type="email" placeholder="{{ __('messages.email') }}" id="email" name="email" value="{{ $email or old('email') }}" required autofocus>

              @if ($errors->has('email'))
                  <div class="help-block">
                      <strong>{{ $errors->first('email') }}</strong>
                  </div>
              @endif
            </div>
          </div>

          <div class="label">
            {{ __('messages.password') }}
          </div>
          <div class="field {{ $errors->has('password') ? ' has-error' : '' }}">
            <div class="control">
              <input type="password" placeholder="{{ __('messages.password') }}" name="password" required>

              @if ($errors->has('password'))
                  <div class="help-block">
                      <strong>{{ $errors->first('password') }}</strong>
                  </div>
              @endif
            </div>
          </div>

          <div class="label">
            {{ __('messages.confirm_password') }}
          </div>
          <div class="field {{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
            <div class="control">
              <input id="password-confirm" type="password" placeholder="{{ __('messages.confirm_password') }}" name="password_confirmation" required>

              @if ($errors->has('password_confirmation'))
                  <div class="help-block">
                      <strong>{{ $errors->first('password_confirmation') }}</strong>
                  </div>
              @endif
            </div>
          </div>

          <div class="field button-field">
              <button class="myloginBTN button is-primary is-large">
                {{ __('messages.login') }}
              </button>
          </div>
          <p class="is-centered"><a @click="show_forgot_password = true"> {{ __('messages.forgot_the_password') }} </a></p>
          <p class="is-centered"><router-link to="/register"> {{ __('messages.create_account') }} </router-link></p>

        </div>
      </form>
      </div>

      <div class="column is-4">
      </div>
  </div>
</div>

@endsection
