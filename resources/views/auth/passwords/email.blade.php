@extends('layouts.app')
@section('content')

<div class="container-fluid" id="login">

  <br><br><br>
  <div>
    <a href="/">
      <h3> {{ __('messages.back_to_home_page') }} </h3>
    </a>
    <br />
    <br />
  </div>
  <div class="columns">

      <div class="column is-4">
      </div>

      <div class="column loginDiv" >

      <form method="POST" action="{{ route('password.email') }}">
          {{ csrf_field() }}
        <h1> {{ __('messages.recover_password') }} </h1>

        @if (session('status'))
        <b-message type="is-success" class="nextCaseWarning" dir="rtl">
            {{ session('status') }}
        </b-message>
        @endif

        <div class="label">
          {{ __('messages.email') }}
        </div>
        <div class="field {{ $errors->has('email') ? ' has-error' : '' }}">
          <div class="control">
            <input type="email" name="email" value="{{ old('email') }}" id="email" placeholder=" {{ __('messages.email') }} " required autofocus>
            @if ($errors->has('email'))
                <div class="help-block">
                    <strong>{{ $errors->first('email') }}</strong>
                </div>
            @endif
          </div>
        </div>

        <div class="field button-field">
            <button class="myloginBTN button is-primary is-large">
              {{ __('messages.send') }}
            </button>
        </div>
      </form>

      </div>

      <div class="column is-4">
      </div>
  </div>
</div>
@endsection
