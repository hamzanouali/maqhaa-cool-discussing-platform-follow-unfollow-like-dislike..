@extends('layouts.app')

@section('content')

<br><br><br><br>
  <div class="container-fluid" id="login">

    <div>
      <a href="/">
        <h3> {{ __('messages.back_to_home_page') }} </h3>
      </a>
      <br>
      <a onclick="history.go(-1)">
        <h3> {{ __('pagination.previous') }} </h3>
      </a>
      <br />
      <br />
    </div>

    <div class="columns">

        <div class="column is-4">
        </div>

        <div class="column is-4" >

        <form class="form-horizontal text-center" method="POST" action="/installer/language/{{ $language }}/{{ $page }}/update">
            {{ csrf_field() }}
          <!-- <p class="is-centered"><img src="/images/icons8_Space_Shuttle_100px.png" alt="إفعلها الآن"></p> -->
          <h1> {{ $page }}({{ $language }}) </h1>

          @if (session('status'))
            <div class="help-block is-success" dir="rtl">
                <strong> {{ session('status') }} </strong>
            </div>
            <br>
          @endif
            
            @foreach($dictionary as $index => $content)

              @if(!is_array($content))
			  
			  <div class="label">
                <strong> {{ str_replace('_',' ',$index) }} </strong>
              </div> 		  
              <div class="field form-group">
                  <div class="control">
                  <input id="text" type="text" placeholder="{{ __('messages.translate_this') }}: {{ $content }}" class="form-control" name="{{ $index }}" value="{{ $content }}" required autofocus>
                  </div>
              </div>
              @else
              <div class="label">
                <strong> {{ __('messages.advenced') }} (JSON) </strong>
              </div> 
              <div class="field form-group">
                <textarea name="{{ $index }}">
                  <?php print_r(json_encode($content)) ?>
                </textarea>
              </div>
              @endif

            @endforeach

            <div class="field button-field">
                <button class="myloginBTN button is-primary is-large">
                {{ __('messages.save') }}
                </button>
            </div>

          </div>
        </form>
        
        </div>

        <div class="column is-4">
        </div>
    </div>
  </div>
@endsection
