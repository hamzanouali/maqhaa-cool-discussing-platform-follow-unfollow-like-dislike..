@extends('layouts.app')

@section('content')

<br><br><br><br>
  <div class="container-fluid" id="login">

    <div>
      <a href="/">
        <h3> {{ __('messages.back_to_home_page') }} </h3>
      </a>
      <br>
      <a onclick="history.go(-1)">
        <h3> {{ __('pagination.previous') }} </h3>
      </a>
      <br />
      <br />
    </div>

    <div class="columns">

        <div class="column is-4">
        </div>

        <div class="column is-4" >

        <form class="form-horizontal text-center" method="POST" action="/create-new-language">
            {{ csrf_field() }}

			<h1> {{ __('messages.add_language') }} </h1>

          @if (session('status'))
            <div class="help-block is-success" dir="rtl">
                <strong> {{ session('status') }} </strong>
            </div>
            <br>
          @endif
            
            @foreach($languages as $index => $content)
          
            <div class="label">
                <a href="/installer/language/{{ $content }}"> {{ $content }} </a> 
                
                @if(count($languages) > 1)
                  <a href="/delete-language/{{ $content }}" 
					class="
					<?php 
						if(
							app()->getLocale() == "ar" ||
							app()->getLocale() == "he" ||
							app()->getLocale() == "pe"
						) { echo "float-left"; }
						else { echo "float-right"; } 
					?>
				  ">
                    {{ __('vue.delete') }}
                  </a>
                @endif  
            </div>   
            <hr>

            @endforeach

            <div class="label">
              {{ __('messages.language') }}
            </div>
            <input type="text" name="language" maxlength="3" placeholder="{{ __('messages.language') }}">

            <div class="field button-field m-t-25">
                <button class="button is-success">
                {{ __('messages.add_language') }}
                </button>
            </div>

          </div>
        </form>
        
        </div>

        <div class="column is-4">
        </div>
    </div>
  </div>
@endsection
