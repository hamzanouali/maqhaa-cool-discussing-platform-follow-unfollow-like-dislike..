@extends('layouts.app')

@section('content')

<br><br><br><br>
  <div class="container-fluid" id="login">

    <div>
      <a href="/">
        <h3> {{ __('messages.back_to_home_page') }} </h3>
      </a>
      <br>
      <a onclick="history.go(-1)">
        <h3> {{ __('pagination.previous') }} </h3>
      </a>
      <br />
      <br />
    </div>

    <div class="columns colors-page">

        <div class="column is-4">
        </div>

        <div class="column is-4" >

        <form class="form-horizontal text-center" method="POST" action="/add-color-to-palette">
            <h1> {{ __('messages.update') }} {{ __('messages.color') }} </h1>

            @if (session('status'))
                <div class="help-block is-success" dir="rtl">
                    <strong> {{ session('status') }} </strong>
                </div>
                <br>
            @endif
            
            <div class="columns is-multiline">

                @foreach($colors as $index => $content)
              
                <div class="column is-4">
                    <a href="/set-color/{{ str_replace('#','',$index) }}/{{ str_replace('#','',$content) }}"> 
                        <span class="myloginBTN button is-primary is-large" style="background:{{ $content }};color:{{ $index }}">
                        {{ __('messages.save') }} 
                        </span>
                    </a>      
                </div>   

                @endforeach
                @foreach($colors as $index => $content)
                
                <div class="column is-4">
                    <a href="/set-color/fffffE/{{ str_replace('#','',$content) }}"> 
                        <span class="myloginBTN button is-primary is-large" style="background:{{ $content }};color:#fffffE">
                        {{ __('messages.save') }} 
                        </span>
                    </a>      
                </div>   

                @endforeach
                @foreach($colors as $index => $content)
                
                <div class="column is-4">
                    <a href="/set-color/707070/{{ str_replace('#','',$content) }}"> 
                        <span class="myloginBTN button is-primary is-large" style="background:{{ $content }};color:#707070">
                        {{ __('messages.save') }} 
                        </span>
                    </a>      
                </div>   

                @endforeach

            </div>

          </div>
        </form>
        
        </div>

        <div class="column is-4">
        </div>
    </div>
  </div>
@endsection
