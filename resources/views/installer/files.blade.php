@extends('layouts.app')

@section('content')

<br><br><br><br>
  <div class="container-fluid" id="login">

    <div>
      <a href="/">
        <h3> {{ __('messages.back_to_home_page') }} </h3>
      </a>
      <br>
      <a onclick="history.go(-1)">
        <h3> {{ __('pagination.previous') }} </h3>
      </a>
      <br />
      <br />
    </div>

    <div class="columns">

        <div class="column is-4">
        </div>

        <div class="column is-4" >

        <form class="form-horizontal text-center">
            {{ csrf_field() }}
          <!-- <p class="is-centered"><img src="/images/icons8_Space_Shuttle_100px.png" alt="إفعلها الآن"></p> -->
          <h1> {{ __('messages.translate_this') }} </h1>

          @if (session('status'))
            <div class="help-block is-success" dir="rtl">
                <strong> {{ session('status') }} </strong>
            </div>
            <br>
          @endif
            
            <div class="help-block">
              <strong>
                {{ __('messages.translate_instructions') }}
              </strong>
            </div>
            @foreach($files as $index => $page)
          
            <div class="label">
                <a href="/installer/language/{{ $language }}/{{ str_replace('.php','',$page) }}"> {{ str_replace('.php','',$page) }} <a>
            </div>   
            <hr>

            @endforeach

          </div>
        </form>
        
        </div>

        <div class="column is-4">
        </div>
    </div>
  </div>
@endsection
