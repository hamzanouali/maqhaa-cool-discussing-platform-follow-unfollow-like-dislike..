@extends('layouts.app')

@section('content')

<br><br><br><br>
  <div class="container-fluid" id="login">

    <div>
      <a href="/">
        <h3> {{ __('messages.back_to_home_page') }} </h3>
      </a>
      <br>
      <a onclick="history.go(-1)">
        <h3> {{ __('pagination.previous') }} </h3>
      </a>
      <br />
      <br />
    </div>

    <div class="columns">

        <div class="column is-4">
        </div>

        <div class="column is-4" >

        <form class="form-horizontal text-center">
            {{ csrf_field() }}

            <h1> {{ __('messages.installer') }} </h1>

            @if (session('status'))
                <div class="help-block is-success" dir="rtl">
                    <strong> {{ session('status') }} </strong>
                </div>
                <br>
            @endif
             
            <div class="label">

                <a href="/installer/language"> {{ __('messages.language') }} </a> 
              
            </div>   
            <hr>
            <div class="label">

                <a href="/installer/theme"> {{ __('messages.theme') }} </a>

            </div>  

          </div>
        </form>
        
        </div>

        <div class="column is-4">
        </div>
    </div>
  </div>
@endsection
