@extends('layouts.app')

@section('content')

<br><br><br><br>
  <div class="container-fluid" id="login">

    <div>
      <a href="/#/">
        <h3> {{ __('messages.back_to_home_page') }} </h3>
      </a>
      <br />
      <br />
    </div>

    <div class="columns">

        <div class="column is-4">
        </div>

        <div class="column is-4" >

        <form class="form-horizontal text-center" method="POST" action="/contact/send">
            {{ csrf_field() }}
          <!-- <p class="is-centered"><img src="/images/icons8_Space_Shuttle_100px.png" alt="إفعلها الآن"></p> -->
          <h1> {{ __('messages.contact_us') }} </h1>

          @if (session('status'))
            <b-message type="is-success" dir="rtl">
                {{ session('status') }}
            </b-message>
          @endif

          <div class="label">
           {{ __('messages.email') }}
          </div>
          <div>
            <div class="field form-group{{ $errors->has('email') ? ' has-error' : '' }}">
              <div class="control">
                <input id="email" type="email" placeholder="{{ __('messages.email') }}" class="form-control" name="email" value="{{ old('email') }}" required autofocus>
                @if ($errors->has('email'))
                    <div class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </div>
                @endif
              </div>
            </div>

            <div class="label">
              {{ __('messages.content') }}
            </div>
            <div>
              <div class="field form-group{{ $errors->has('content') ? ' has-error' : '' }}">
                <div class="control">
                  <textarea maxlength="700" id="content" class="form-control" name="content" value="{{ old('content') }}" required>
                  </textarea>
                  @if ($errors->has('content'))
                      <div class="help-block">
                          <strong>{{ $errors->first('content') }}</strong>
                      </div>
                  @endif
                </div>
              </div>

            <div class="field button-field">
                <button class="myloginBTN button is-primary is-large">
                {{ __('messages.send') }}
                </button>
            </div>

          </div>
        </form>
        </div>

        <div class="column is-4">
        </div>
    </div>
  </div>
@endsection
