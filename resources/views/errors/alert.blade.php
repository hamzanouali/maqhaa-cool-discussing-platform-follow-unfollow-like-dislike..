@extends('layouts.app')

@section('content')

<br><br><br><br><br><br>
  <div class="container-fluid" id="login">
    <div>

      @if(\Auth::check())
      <a href="/logout">
        <h3> {{ __('messages.logout') }} </h3>
      </a>
      @else
      <a href="/">
        <h3> {{ __('messages.back_to_home_page') }} </h3>
      </a>
      @endif

      <br />
      <br />
    </div>
    <div class="columns">

        <div class="column is-4">
        </div>

        <div class="column" >
          @if(\Auth::check())
            @if(\Auth::user()['is_active'] == 0)
            <div class="help-block is-success" dir="rtl">
                <strong> {{ \Auth::user()['name'] }} : {{ __('messages.activate_your_account') }} </strong>
            </div>
            @elseif(\Auth::user()['is_active'] == 2)
            <div class="help-block">
                <strong> {{ \Auth::user()['name'] }} : {{ __('messages.blocked_account') }} </strong>
            </div>
            @endif
          @endif
        </div>
        
        <br />
        <a href="/contact">
          <span> {{ __('messages.contact_us') }} </span>
        </a>

        <div class="column is-4">
        </div>
    </div>
  </div>
@endsection
