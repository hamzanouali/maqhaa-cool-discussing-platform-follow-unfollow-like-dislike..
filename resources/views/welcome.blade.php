<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <base href="/" />
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <!-- *** [META TAGS FOR SEO] *** -->

        <meta name="description" content=" ">
        <meta name="keywords" content=" ">
        <meta name="author" content=" ">

        <!-- *** [BROWSER TAB ICONS] *** -->

        <link rel="icon" sizes="24x24" type="image/png" href="/images/icon.png">

        <!-- =========================== -->
        
        <?php if(isset(explode('/',$_SERVER['REQUEST_URI'])[4])) { ?>
          <title> {!! urldecode(str_replace('-',' ',explode('/',$_SERVER['REQUEST_URI'])[4])) !!} </title>
        <?php } else { ?>
          <title> {!! config('app.name') !!} </title>
        <?php } ?>

        <!-- styling  -->
        <link rel="stylesheet" href="/css/app.css">
        
    </head>
    <body class="">

      <!--
      this message will be shown when the user disable JavaScript
      -->
      <noscript>
        <div class="help-block no-js-alert">
          <strong>
            <h1> <strong> ـــ WARNING ـــ </strong> </h1>
            This website not works without JavaScript. Here are the
            <a href="http://www.enable-javascript.com" target="_blank">
              instructions how to enable JavaScript in your web browser
            </a>
          </strong>
        </div>
      </noscript>

      <!-- The fake page will be shown while loading the content -->
      <div id="FakePage" class="women_street_style">
        <div class="container-fluid navbar-top">
          <div class="container">
            <div class="columns">
              <div class="column is-1"></div>
              <div class="column">
                <span><i class="fas fa-cubes"></i> <span class="example"> example </span> </span>
              </div>
              <div class="column is-1"></div>
            </div>
          </div>
        </div>
        <br><br><br>
        <div class="container">
          <div class="container">
            <div class="columns">
              <div class="column is-1"></div>
              <div class="column is-10">

                <!--  -->
                <div class="columns">
                  <div class="column is-3">

                    <div class="side-articles">
                      <div class="title">
                        <span class="example">
                          example
                        </span>
                      </div>
                      <div class="body">
                        <ul>
                          <li v-for="n in 2">
                            <span class="example">
                              example example
                            </span>
                          </li>
                        </ul>
                      </div>
                    </div>
                  </div>
                  <div class="column is-6">

                    <!-- post -->
                    <div class="wait-loading ws-div m-b-50">
                      <div class="header">
                        <span class="user"> example </span>
                        <span class="time"> example </span>
                      </div>
                      <div class="body">
                        <!-- empty -->
                      </div>
                      <p class="content">
                        .
                      </p>
                      <div class="after-body">
                        <!-- <span class="in"> في </span> -->
                        <span class="category"> #example</span>
                      </div>
                      <div class="footer columns">
                        <div class="column">
                          <span class="comments"></span>
                        </div>
                      </div>
                    </div>

                  </div>
                  <div class="column is-3">

                    <div class="side-articles">
                      <div class="title">
                        <span class="example">
                          example
                        </span>
                      </div>
                      <div class="body">
                        <ul>
                          <li v-for="n in 2">
                            <span class="example">
                              example example
                            </span>
                          </li>
                        </ul>
                      </div>
                    </div>

                  </div>
                </div>
                <!--  -->

              </div>
              <div class="column is-1"></div>
            </div>
          </div>
        </div>

      </div>
        

      <!-- Vuejs app container -->
      @if(
        app()->getLocale() == 'ar' ||
        app()->getLocale() == 'he' ||
        app()->getLocale() == 'pe'
      )
      <div id="app" class="women_street_style">
      @else 
      <div id="app" class="women_street_style ltr_style">
      @endif
        <router-view></router-view>
      </div>    
           
      <script>   

        /*** [ SET DEFAULT LANGUAGE ] ***/

        window.default_laguage = 'en';

        /*** [ GOOGLE ANALYTICS APP KEY ] ***/

        window.google_analytics_app_key = 'AA-XXXXXXXXX-X';


        /*** [ DEVELOPERS AREA ] ***/
        /*

          WARNING: if you can't understand the code below, don't touche it please.
          Email: nouali.hamza.1998@gmail.com
          Feel free to email me.

        */

        // get all folders in /resources/lang
        let folders_in_lang = <?php echo json_encode(array_slice(scandir(base_path().'/resources/lang'),2)) ?>; 
        // init dictionary
        window.dictionary = {};
        // push available languages to dictionary  
        folders_in_lang.forEach(val => { 
          window.dictionary[val] = {}
        });
        // push the dictionary of the locale language 
        window.dictionary['<?php echo app()->getLocale() ?>'] = <?php echo json_encode(__('vue')) ?>;

      </script>
      <script src="https://unpkg.com/pretty-scroll@1.1.0/js/pretty-scroll.js"></script>
          
      <script src="/js/manifest.js"></script>
      <script src="/js/vendor.js"></script>
      <script src="/js/app.js"></script>
        
      
    </body>
</html>
