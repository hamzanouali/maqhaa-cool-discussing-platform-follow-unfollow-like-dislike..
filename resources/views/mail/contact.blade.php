@component('mail::message')


  {{ __('messages.sender') }}:

<hr>
{{ $email }}
<br>

  {{ __('messages.content') }}:

<br>

{{ $content }}

<br>
<br>
{{ config('app.name') }}
@endcomponent
