@component('mail::message')
# {{ __('messages.account_activation_header') }}


{{ __('messages.account_activation_body') }}

@component('mail::button', ['url' => $url])
{{ __('messages.account_activation_button') }}
@endcomponent
<br>
{{ $url }}
<br>
{{ __('messages.reset_password_regards') }}
<br>
{{ config('app.name') }}
@endcomponent
