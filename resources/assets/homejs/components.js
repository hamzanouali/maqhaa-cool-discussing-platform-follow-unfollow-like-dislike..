/*

Feel free to email me if you face some problems.

Email: nouali.hamza.1998@gmail.com
Author: Hamza Nouali

*/

import dashboard from './dashboard.vue';

Vue.component('dashboard',dashboard);


Vue.component('navbar',{
  template: `

  <div class="root">
    <div class="container-fluid navbar-top">

      <!-- language dropdown -->
      <div v-if="$store.getters.languages.length > 1" class="is-hidden-mobile fixed-language-dropdown">
        <b-dropdown position="is-top-left">
            <span slot="trigger">
                <span><i class="fas fa-language"></i></span>
            </span>

            <b-dropdown-item v-for="elem in $store.getters.languages">
              <a :href="'/set-language/'+elem"> {{ elem }} </a>
            </b-dropdown-item>
        </b-dropdown>
      </div>
      <div v-if="$store.getters.languages.length > 1" class="is-hidden-desktop fixed-language-dropdown">
        <b-dropdown>
            <span slot="trigger">
                <span><i class="fas fa-language"></i></span>
            </span>

            <b-dropdown-item v-for="elem in $store.getters.languages">
              <a :href="'/set-language/'+elem"> {{ elem }} </a>
            </b-dropdown-item>
        </b-dropdown>
      </div>
      <!-- language dropdown -->

      <div class="container">
        <div class="columns">
          
          <div class="column is-1"></div>
          <div class="column is-10">

            <div @click="this.window.location.href='/'" class="float-left login-user">
              <span><i class="far fa-home"></i> {{ $store.getters.lang.back_to_home_page }} </span>
            </div>
            
          </div>
          <div class="column is-1"></div>
        </div>
      </div>

    </div>

  </div>

  `
});


Vue.component('text-auto-direction-rtl-ltr',{
  props: ['title','id'],
  template: `

  <div ref="textAutoDirection" class="body">
    <router-link :to="{ name: 'showPublication', params: { id : id, title: title.replace(/[\/\\\s]/g,'-')} }">
      {{ title }}
    </router-link>
  </div>

  `,

  mounted(){

    let dir = this.$refs.textAutoDirection;

    // ASCII
    // RTL characters start by 256

  	if (this.title.charCodeAt(0) > 255) {
      dir.style.direction = 'rtl';
  		dir.style.textAlign = 'right';
  	}
  	else {
  		dir.style.direction = 'ltr';
      dir.style.textAlign = 'left';
  	}

  }
});

Vue.component('update-quill-content',{
  props: ['topic','publication_title','category','comment_id'],
  template: `
  <div class="update-quill-content columns m-t-15 is-multiline is-mobile">
    <a class="smallUnderlinedText" @click="open_modal='display:block'"> {{ $store.getters.lang.edit }} </a>
    <a class="smallUnderlinedText m-r-10" @click="removepublicationOrComment($route.params.id)"> {{ $store.getters.lang.delete }} </a>

    <div :style="open_modal" class="column is-12 modal">
      <div class="modal-background" @click="open_modal='display:none'"></div>
      <div class="modal-card m-t-30" style="margin:auto">

        <section class="modal-card-body">
          <!-- Content ... -->

          <div class="columns is-multiline">
            <div v-if="this.publication_title" class="column is-12 post-label">
              <label> {{ $store.getters.lang.title }}: </label>
            </div>
            <div v-if="this.publication_title" class="column is-12 m-t-0 p-t-0">
              <input placeholder="..." type="text" v-model="title">
            </div>

            <div v-if="this.category" class="column is-12">
              <!-- category -->
              <b-field
                  :label="$store.getters.lang.category">
                  <b-select v-model="selected_category" class="select-cat" :placeholder="$store.getters.lang.category" expanded>
                      <option v-for="cat in categories" :value="cat.id">{{ cat.category }}</option>
                  </b-select>
              </b-field>
            </div>
            <div class="column is-12 post-label m-t-25">
              <label> {{ $store.getters.lang.text }}: </label>
            </div>
            <div class="column is-12 p-t-0 m-t-0">
              <div ref="QuillArea"></div>
            </div>
          </div>


          <!-- Content ... -->
        </section>
        <footer style="background:#1b1b1b;" dir="ltr" class="modal-card-foot">
          <button ref="submitBTN" class="button is-primary" @click="update()"> {{ $store.getters.lang.update }} </button>
          <button class="button is-light" @click="open_modal='display:none'"> {{ $store.getters.lang.cancel }} </button>
        </footer>
      </div>
    </div>
  </div>
  `,

  data(){
    return {
      open_modal: 'display:none',
      title: this.publication_title,
      selected_category: this.category,
      quill:null,
      categories: [],
    }
  },

  mounted(){

    this.quill = new Quill(this.$refs.QuillArea, {
      modules: {
        syntax: window.syntax,
        toolbar: window.quill_tollbar,
      },
      theme: 'snow',
      formats : window.quill_formats
    });
    this.quill.setContents(JSON.parse(this.topic));

    if(this.category) this.getCategories();
  },
  watch: {
    open_modal(val){
      if(val == 'display:none'){
        this.quill.setContents(JSON.parse(this.topic));
      }
    }
  },
  methods: {

    getCategories(){
        axios.get('/get_old_categories')
        .then(res => {
          this.categories = res.data;
        })
        .catch(err => {
          //
        });
    },

    removepublicationOrComment(id){
      if(this.publication_title && this.category){
        this.$dialog.confirm({
            title: this.$store.getters.lang.action_delete,
            message: this.$store.getters.lang.do_you_want_to_continue_this_action,
            confirmText: this.$store.getters.lang.delete,
            cancelText: this.$store.getters.lang.cancel ,
            type: 'is-danger',
            hasIcon: false,
            onConfirm: () => {
              axios.post('/remove_publication/'+id)
              .then(res => {
                if(res.data == true) {
                  this.$toast.open({
                      duration: 5000,
                      message: this.$store.getters.lang.post_has_been_deleted,
                      position: 'is-bottom',
                      type: 'is-dark'
                  });
                }else if(res.data == '401-forbidden'){
                  this.$toast.open({
                      duration: 5000,
                      message: this.$store.getters.lang.not_allowed_to_continue_this_action,
                      position: 'is-bottom',
                      type: 'is-dark'
                  });
                }
                this.$router.push('/');
              })
              .catch(err => {

              });
            }
        });
      }else{
        this.$dialog.confirm({
            title: this.$store.getters.lang.action_delete,
            message: this.$store.getters.lang.do_you_want_to_continue_this_action,
            confirmText: this.$store.getters.lang.delete,
            cancelText: this.$store.getters.lang.cancel,
            type: 'is-danger',
            hasIcon: false,
            onConfirm: () => {
              axios.post('/remove_comment/'+this.comment_id)
              .then(res => {
                if(res.data == true) {
                  this.$toast.open({
                      duration: 5000,
                      message: this.$store.getters.lang.post_has_been_deleted,
                      position: 'is-bottom',
                      type: 'is-dark'
                  });
                  document.getElementById('comment'+this.comment_id).style.display= 'none';
                }else if(res.data == '401-forbidden'){
                  this.$toast.open({
                      duration: 5000,
                      message: this.$store.getters.lang.not_allowed_to_continue_this_action,
                      position: 'is-bottom',
                      type: 'is-dark'
                  });
                }else {
                  this.$toast.open({
                      duration: 5000,
                      message: this.$store.getters.lang.error,
                      position: 'is-bottom',
                      type: 'is-dark'
                  });
                }

              })
              .catch(err => {

              });
            }
        });
      }
    },

    update(){
      this.$refs.submitBTN.className='button is-primary is-loading';
      // publication case
      if(this.publication_title && this.category){
        axios.post('/update_a_publication/'+this.$route.params.id,{
          user_id: this.$store.getters.user.id,
          title : this.title,
          category_id : this.selected_category,
          topic: JSON.stringify(this.quill.getContents()),
          topic_text : this.quill.getText()
        }).then(res => {
          if(res.status == 200) {
            this.open_modal = 'display:none';
            this.$refs.submitBTN.className='button is-primary';
            this.$emit('changed',{
              title : this.title,
              category : this.selected_category,
              topic: JSON.stringify(this.quill.getContents())
            });
            // window.location.reload();
          }
        })
        .catch(err => {
          this.$refs.submitBTN.className='button is-primary';
          this.$snackbar.open({
              message: this.$store.getters.lang.be_sure_to_fill_all_inputs,
              type: 'is-danger',
              position: 'is-bottom',
              actionText: this.$store.getters.lang.close,
              indefinite: true,
              onAction: () => {

              }
          });
        });

      // comment case
      }else{
        axios.post('/update_a_comment/'+this.comment_id,{
          user_id: this.$store.getters.user.id,
          topic: JSON.stringify(this.quill.getContents()),
          topic_text : this.quill.getText()
        }).then(res => {
          if(res.status == 200) {
            this.open_modal = 'display:none';
            this.$refs.submitBTN.className='button is-primary';
            this.$emit('changed',{
              topic: JSON.stringify(this.quill.getContents())
            });
          }
        })
        .catch(err => {
        });
      }

    }
  }

});

Vue.component('Quill-bubble-show',{
  props: ['topic'],
  template: `
    <div class="root p-0">
      <div class="p-0" ref="show"></div>
    </div>
  `,
  data(){
    return {
      quill: null
    }
  },

  watch: {
    topic:function(val){
      if(this.quill.getText().length){
        this.quill.setContents(JSON.parse(val));
      }
    }
  },

  mounted(){

    // create and bound the qill text editor to the div with ref="show"
    this.quill = new Quill(this.$refs.show, {
      theme: 'bubble',
      readOnly: true,
      modules: {
        syntax: window.syntax
      },
    });
    this.quill.setContents(JSON.parse(this.topic));

  }
});

Vue.component('Quill-Snow-Editor',{
  template: `
    <div class="root p-0">
      <div class="p-0" ref="show"></div>
    </div>
  `,

  data(){
    return {
      quill: null,
    }
  },

  mounted(){

    // create and bound the qill text editor to the div with ref="show"
    this.quill = new Quill(this.$refs.show, {
      theme: 'snow',
      readOnly: true,
      modules: {
        syntax: window.syntax,              // Include syntax module
        toolbar: window.quill_tollbar  // Include button in toolbar
      },
      formats : window.quill_formats
    });

    let interval = setInterval(()=>{
      if(this.topic) {
        this.quill.setContents(JSON.parse(this.topic));
        clearInterval(interval);
      }
    },10);
  }

});

Vue.component('like-btn',{
  props: ['target_type','target_id','user_id','value'],
  template: `
    <div class="column like-dislike m-b-15">
      <span ref="like_btn"> {{ formatString(get_value) }} </span>
      <i @click=" data_to_send.action = 'like';sendToDB()" class="far fa-thumbs-up m-l-5"></i>
      <i @click=" data_to_send.action = 'dislike';sendToDB()" class="far fa-thumbs-down"></i>
    </div>
  `,
  data(){
    return {
        get_value: this.value,
        data_to_send : {
          user_id : this.user_id,
          action: null,
          target_type: this.target_type,
          target_id: this.target_id
        }
    }
  },
  methods: {
    sendToDB(){
      if(!this.$store.getters.user.id){
        this.$snackbar.open({
            message: this.$store.getters.lang.login_first,
            type: 'is-warning',
            position: 'is-bottom',
            actionText: this.$store.getters.lang.close,
            indefinite: true,
            duration: 999999,
            onAction: () => {
            }
        });
        return;
      }else if(this.$store.getters.user.id == this.user_id){
        this.$snackbar.open({
            message: this.$store.getters.lang.you_cant_rate_your_posts,
            type: 'is-warning',
            position: 'is-bottom',
            actionText: this.$store.getters.lang.close,
            indefinite: true,
            onAction: () => {
            }
        })
        return;
      }

      axios.post('/like-dislike',this.data_to_send)
        .then(res => {
          if(res.data == '401-dislike'){
            // check for authorization
            if(this.data_to_send.action == 'dislike' && this.$store.getters.user.reputation < 250){
              this.$snackbar.open({
                  message: this.$store.getters.lang.not_allowed_to_continue_this_action,
                  type: 'is-warning',
                  position: 'is-bottom',
                  indefinite: true,
                  onAction: () => {
                  }
              })
            }
          }else if(res.data == '401-owner'){ /**/ }
          else{
            if(!res.data && res.data !== 0) res.data = this.$refs.like_btn.innerText;
            this.$refs.like_btn.innerText = res.data;
          }
        }).catch(err => {

        });
    },

    formatString(value){
      if(value == 0) return 0;
      if(value < 1000) return value;

      value = ''+value+'';
      let new_value = '';
      for (var i = 0; i < value.length; i++) {
        if((i%3)== 0 && i != value.length-1) new_value += value.charAt(i)+',';
        else new_value += value.charAt(i);
      }
      return new_value;
    },
  }
});
