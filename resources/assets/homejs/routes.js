import VueRouter from 'vue-router';
import { store } from './store/store';

// import moment.js and set the locale language to Arabic (Algeria)
var moment = require('moment');

window.moment = moment;

// get all cookies and convert it to json
// just to get the saved language
let getCookiesAsJson = ()=>{

  let cookie_str = document.cookie;
  cookie_str = cookie_str.replace(/;/g,'","');
  cookie_str = cookie_str.replace(/=/g,'":"');
  cookie_str = cookie_str.replace(/\s/g,'');
  cookie_str = '{"'+cookie_str+'"}';
  return JSON.parse(cookie_str);

};


// in case that the template lanuage already saved in a cookie
if(getCookiesAsJson().hasOwnProperty('language')) {

  // change template language
  store.commit('language',getCookiesAsJson().language);

// else
}else{

  // save the language in a cookie
  document.cookie = "language="+store.getters.language+"; expires= Infinity";

}

// set the moment js locale
// example: 5 days ago -> english (en)
moment.locale(store.getters.language); 

let routes = [

  {
    path : '/dashboard/',
    redirect: '/dashboard/'+store.getters.language
  },

  {
    path : '/dashboard/:language',
    name: 'dashboard',
    beforeEnter: (to,form,next)=>{
      // check wether the passed param to :language does not exists in dictionary
      if(store.getters.state_dictionary.hasOwnProperty(to.params.language)) return next();
      else return next(store.getters.language + '/404-page-not-found');
    },
    component : require('./user/home'),
  },

  {
    path : '/dashboard/create-categories',
    name : 'createCategories',
    component : require('./user/create_cat')
  },

  {
    path: '/dashboard/users',
    name: 'users',
    component: require('./user/users')
  },

  {
    path: '/dashboard/questions',
    name: 'dashQuestions',
    component: require('./user/questions')
  },

  {
    path: '/dashboard/comments',
    name: 'dashComments',
    component: require('./user/comments')
  },

  {
    path : '/dashboard/privacy',
    name : 'privacy',
    component : require('./privacy')
  },

  {
    path : '/dashboard/posting-rules',
    name : 'pRules',
    component : require('./postingRules')
  },

];

/////////////////////////////////////////////////
// to avoid '/:language' repeatation in each path
/////////////////////////////////////////////////
for (var i = 2; i < routes.length; i++) {
  routes[i].path = routes[i].path.replace('/dashboard/','/dashboard/:language/');
}
/////////////////////////////////////////////////

export default new VueRouter({
  // this code for jumping in the same page using #to-tag
  scrollBehavior: function(to, from, savedPosition) {
      if (to.hash) {
          return {selector: to.hash}
      } else {
          return { x: 0, y: 0 }
      }
  },
  mode: 'history',
  routes
});
