require('./bootstrap.js');
import router from './routes.js';
import { store } from './store/store';
require('./components.js');
import routesHandler from './routes_handler';

/*
------------------------------------------------------------------------------------------
here i am catching all the 'matas' from the current route that i am trying to enter it now
and then do what should i do.
for example: if the route require authentication, i will check if i am logged in
or i will redirect myself to the login page.
------------------------------------------------------------------------------------------
*/

router.beforeEach( (to,from,next)=>{
  NProgress.start();

  // if the path does not contain the lanugage example : http://domain.com/users/000/Name
  // will be redirected to : http://domain.com/en/users/000/Name
  if(!to.params.hasOwnProperty('language'))
    return next('/'+store.getters.language+to.path);

  /*
    the chosen language from the user in the path for example: http://domain.com/en/...
    is not the same as our template default language: 'ar'
  */
  if(to.params.language != store.getters.language){
    store.commit('language',to.params.language);
  }

  // 404 page handler
  if(to.matched.length == 0) {
    return next("/"+store.getters.language+"/404-page-not-found");
  }

  NProgress.set(0.6);

  // save the last route
  store.commit('last_route',to);


  let obj = new routesHandler(to,from,next);
  obj.checkForAuthentication();

  // count the not readed notification
  if(store.getters.user.id) {
    axios.get('/count-not-readed-notifications')
    .then(res => {
      store.commit('notifications',res.data);
    });
  }

} );

router.afterEach((to, from) => {
  // put the title in the browser tab
  if(to.params.title){
    document.title = to.params.title.replace(/-/g,' ');
  }
  NProgress.done();
});

new Vue({
  beforeCreate(){
    if(store.getters.fromState_isAuth == ''){
      axios.get('/isAuth')
      .then(res => {

        if(res.data.id) {
          store.commit('fromState_isAuth',true);
          store.commit('user',res.data);

          if(res.data.is_active && res.data.is_active == '2'){
            this.$snackbar.open({
                duration : 999999999,
                message: ' حسابك محظور، ليس بإمكانك النشر أو التفاعل بالموقع بعد الآن ',
                type: 'is-danger',
                position: 'is-bottom',
                actionText: 'إغلاق',
                indefinite: true,
                onAction: () => {
                    // this.$toast.close();
                }
            });
          }else if(res.data.is_active && res.data.is_active == '0'){
            this.$snackbar.open({
                duration : 999999999,
                message: ' أرسلنا رابط تفعيل حسابك إلى بريدك الإلكتروني ',
                type: 'is-warning',
                position: 'is-bottom',
                actionText: 'إغلاق',
                indefinite: true,
                onAction: () => {
                    // this.$toast.close();
                }
            });
          }

          // count the not readed notification
          if(store.getters.user.id) {
            axios.get('/count-not-readed-notifications')
            .then(res => {
              store.commit('notifications',res.data);
            });
          }

        }else{
          store.commit('fromState_isAuth',false);
        }
      });
    }
  },
  el : '#app',
  router,
  store,
});

/**
 * Uncomment below when compiling to production
 */
// Vue.config.devtools = false;
// Vue.config.debug = false;
// Vue.config.silent = true;
