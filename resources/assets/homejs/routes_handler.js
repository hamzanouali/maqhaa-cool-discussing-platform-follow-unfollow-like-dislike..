require('./bootstrap.js');
import router from './routes.js';
import { store } from './store/store';

export default class routesHandler {

  // to_object is specified by the beforeEach router's method
  constructor(to_object,from_object,next_object){
    this.to = to_object;
    this.from = from_object;
    this.next = next_object;
  }

  // check for the authorization by role (ex: admin)
  // return boolean
  checkForAuthorizationByRole(){
    if(this.to.meta.admin == true){
      if(store.getters.user.role === undefined) {
        // NProgress.start();
        let counter = 500;
        let interval = setInterval( () => {
          if(counter == 10000 || store.getters.user.role != undefined && store.getters.user.role != 1) {
            clearInterval(interval);
            // not authorized
            return this.next('/404-page-not-found');
          }else if(store.getters.user.role == 1) {
            clearInterval(interval);
            // authorized
            return this.next();
          }
          counter += 500;
        },500);
      }
      else if(store.getters.user.role == 1){
        return this.next();
      }else {
        // not authorized
        return this.next('/404-page-not-found');
      }
    }
  }

  // check for authentication
  // return boolean
  checkForAuthentication(){
    NProgress.set(0.3);
    if(this.to.meta.isAuth == true){
      store.commit('pageRequireAuth',true);
      this.checkForAuthorizationByRole();

      let counter = 500;
      let interval = setInterval( () => {
        if(store.getters.user.id > 0) {
          clearInterval(interval);
          // authorized
          return this.next();
        }

        if(!store.getters.user.id && counter == 5000) window.location.href = '/login';
        counter += 500;
      },500);

    }else{
      return this.next();
    }

  }

}
