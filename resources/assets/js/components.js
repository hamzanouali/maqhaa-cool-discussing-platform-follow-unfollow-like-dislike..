/*

Feel free to email me if you face some problems.

Email: nouali.hamza.1998@gmail.com
Author: Hamza Nouali

------------------------------------------------

Welcome here :)

Note:
- <f-reply> component is a little bit complicated, so don't
  touche any code in it before you understand it very well.

*/


// async component
Vue.component('publications',(resolve, reject) => {
  return import('./views/publications/publications.vue');
});

Vue.component('join-box',{
  props: ['user_obj'],
  template : `
  <div class="join-box">

    <!-- auth user  -->
    <div v-if="$store.getters.user.id && $route.name != 'showPublication'">
      <div class="profile-photo1">
        <div>
          <img class="cursor-pointer" @click="$router.push('/users/'+$store.getters.user.id+'/'+$store.getters.user.name.replace(/[\/\\\s]/g,'-'))" :src="'https://www.gravatar.com/avatar/'+$store.getters.user.profile_img+'?d='+window.default_user_photo" />
        </div>
      </div>
      <p class="cursor-pointer" @click="$router.push('/users/'+$store.getters.user.id+'/'+$store.getters.user.name.replace(/[\/\\\s]/g,'-'))" >{{ $store.getters.user.name }}</p>
      <span> {{ $store.getters.lang.welcome }} </span>
    </div>

    <div v-if="user_obj && $route.name == 'showPublication'">
      <div class="profile-photo1">
        <div>
          <img class="cursor-pointer" @click="$router.push('/users/'+user_obj.id+'/'+user_obj.name.replace(/[\/\\\s]/g,'-'))" :src="'https://www.gravatar.com/avatar/'+user_obj.profile_img+'?d='+window.default_user_photo" />
        </div>
      </div>
      <p class="cursor-pointer" @click="$router.push('/users/'+$store.getters.user.id+'/'+user_obj.replace(/[\/\\\s]/g,'-'))" >{{ user_obj.name }}</p>
      <span> {{ $store.getters.lang.publisher }} </span>
    </div>

    <div v-if="!$store.getters.user.id && $route.name != 'showPublication'">
      <p v-if="!$store.getters.user.id">
        {{ $store.getters.lang.register_message }}
      </p>
      <button @click="this.window.location.href='/register'" class="button is-primary"> {{ $store.getters.lang.join_now }} </button>
    </div>

  </div>
  `,
  data(){
    return {
      window
    }
  }
});

Vue.component('privacy-contact',{
  template: `
  <div class="special-box side-articles">
    <div @click="this.window.location.href='/contact'" class="title">
      {{ $store.getters.lang.contact_us }}
    </div>
    <div @click="$router.push('/privacy')" class="title">
      {{ $store.getters.lang.privacy }}
    </div>
  </div>
  `
});

Vue.component('navbar',{
  template: `

  <div class="root">
    <div class="container-fluid navbar-top">

      <!-- language dropdown -->
      <div v-if="$store.getters.languages.length > 1" class="is-hidden-mobile fixed-language-dropdown">
        <b-dropdown position="is-top-left">
            <span slot="trigger">
                <span><i class="fas fa-language"></i></span>
            </span>

            <b-dropdown-item v-for="elem in $store.getters.languages">
              <a :href="'/set-language/'+elem"> {{ elem }} </a>
            </b-dropdown-item>
        </b-dropdown>
      </div>
      <div class="is-hidden-desktop fixed-language-dropdown">
        <b-dropdown>
            <span slot="trigger">
                <span><i class="fas fa-language"></i></span>
            </span>

            <b-dropdown-item v-for="elem in $store.getters.languages">
              <a :href="'/'+elem"> {{ elem }} </a>
            </b-dropdown-item>
        </b-dropdown>
      </div>
      <!-- language dropdown -->

      <div class="container">
        <div class="columns">
          <span v-if="$route.name != 'home'" class="back-home-page is-hidden-mobile">
            <router-link to="/">
              <i class="fas fa-arrow-right"></i>
            </router-link>
          </span>
          <div class="column is-1"></div>
          <div class="column is-10">

            <div v-if="!$store.getters.user.id" @click="this.window.location.href='/login'" class="float-left login-user">
              <span><i class="far fa-user-circle"></i> {{ $store.getters.lang.login }} </span>
            </div>
            <div v-if="$store.getters.user.id" @click="this.window.location.href='/logout'" class="float-left login-user">
              <span><i class="fas fa-sign-out-alt"></i> {{ $store.getters.lang.logout }} </span>
            </div>

            <!-- Desktop -->
            <span class="is-hidden-desktop is-hidden-tablet">
              <router-link :to="{ name : 'categories' }">
                <i class="fas fa-cubes"></i>
              </router-link>
            </span>

            <span class="is-hidden-desktop is-hidden-tablet">
              <router-link :to="{ name : 'followCategories' }">
                <i class="fas fa-rss"></i>
              </router-link>
            </span>

            <span class="notifications is-hidden-desktop is-hidden-tablet">
              <router-link :to="{ name : 'notifications' }">
                <i class="fas fa-bell is-hidden-tablet"></i>
                <i class="text">0</i>
              </router-link>
            </span>

            <span class="notifications is-hidden-desktop is-hidden-tablet">
              <router-link :to="{ name : 'post_a_publication' }">
                <i class="fas fa-pencil-alt"></i>
              </router-link>
            </span>

            <!-- Mobile -->
            <span class="is-hidden-desktop is-hidden-tablet">
              <router-link :to="{ name : 'home' }">
                <i class="fas fa-arrow-right"></i>
              </router-link>
            </span>

            <ul>
              <li class="is-hidden-mobile">
                <router-link :to="{ name : 'categories' }">
                  <span>
                    <i class="fas fa-cubes"></i>
                  </span>
                  <span>
                    {{ $store.getters.lang.categories }}
                  </span>
                </router-link>
              </li>

              <li class="is-hidden-mobile">
                <router-link :to="{ name : 'followCategories' }">
                  <span>
                    <i class="fas fa-rss"></i>
                  </span>
                  <span>
                    {{ $store.getters.lang.follow_more }}
                  </span>
                </router-link>
              </li>

              <li class="notifications is-hidden-mobile">
                <router-link :to="{ name : 'notifications' }">
                  <span>
                    <i class="fas fa-bell"></i>
                    <i v-if="$store.getters.notifications" class="text">{{ $store.getters.notifications }}</i>
                  </span>
                  <span>
                    {{ $store.getters.lang.notifications }}
                  </span>
                </router-link>
              </li>

              <li class="notifications is-hidden-mobile">
                <router-link :to="{ name : 'post_a_publication' }">
                  <span>
                    <i class="fas fa-pencil-alt"></i>
                  </span>
                  <span>
                    {{ $store.getters.lang.post }}
                  </span>
                </router-link>
              </li>
            </ul>

          </div>
          <div class="column is-1"></div>
        </div>
      </div>

    </div>

    <div class="m-t-60">
      <div v-if="$route.name != 'home'" class="container logo-container">
        <div class="columns">

          <div class="column is-1"></div>
          <div class="column is-10">
            <img @click="$router.push('/')" id="logo" src="/images/logo/logo5.png" />
          </div>
          <div class="column is-1"></div>

        </div>
      </div>
    </div>

  </div>

  `
});


Vue.component('text-auto-direction-rtl-ltr',{
  props: ['title','id'],
  template: `

  <div ref="textAutoDirection" class="body">
    <router-link :to="{ name: 'showPublication', params: { id : id, title: title.replace(/[\/\\\s]/g,'-')} }">
      {{ title }}
    </router-link>
  </div>

  `,

  mounted(){

    let dir = this.$refs.textAutoDirection;

    // ASCII
    // RTL characters start by 256

  	if (this.title.charCodeAt(0) > 255) {
      dir.style.direction = 'rtl';
  		dir.style.textAlign = 'right';
  	}
  	else {
  		dir.style.direction = 'ltr';
      dir.style.textAlign = 'left';
  	}

  }
});

Vue.component('update-quill-content',{
  props: ['topic','publication_title','category','comment_id'],
  template: `
  <div class="update-quill-content columns m-t-15 is-multiline is-mobile">
    <a class="smallUnderlinedText" @click="open_modal='display:block'"> {{ $store.getters.lang.edit }} </a>
    <a class="smallUnderlinedText m-r-10" @click="removepublicationOrComment($route.params.id)"> {{ $store.getters.lang.delete }} </a>

    <div :style="open_modal" class="column is-12 modal">
      <div class="modal-background" @click="open_modal='display:none'"></div>
      <div class="modal-card m-t-30" style="margin:auto">

        <section class="modal-card-body">
          <!-- Content ... -->

          <div class="columns is-multiline">
            <div v-if="this.publication_title" class="column is-12 post-label">
              <label> {{ $store.getters.lang.title }}: </label>
            </div>
            <div v-if="this.publication_title" class="column is-12 m-t-0 p-t-0">
              <input placeholder="..." type="text" v-model="title">
            </div>

            <div v-if="this.category" class="column is-12">
              <!-- category -->
              <b-field
                  :label="$store.getters.lang.category">
                  <b-select v-model="selected_category" class="select-cat" :placeholder="$store.getters.lang.category" expanded>
                      <option v-for="cat in categories" :value="cat.id">{{ cat.category }}</option>
                  </b-select>
              </b-field>
            </div>
            <div class="column is-12 post-label m-t-25">
              <label> {{ $store.getters.lang.text }}: </label>
            </div>
            <div class="column is-12 p-t-0 m-t-0">
              <div ref="QuillArea"></div>
            </div>
          </div>


          <!-- Content ... -->
        </section>
        <footer style="background:#1b1b1b;" dir="ltr" class="modal-card-foot">
          <button ref="submitBTN" class="button is-primary" @click="update()"> {{ $store.getters.lang.update }} </button>
          <button class="button is-light" @click="open_modal='display:none'"> {{ $store.getters.lang.cancel }} </button>
        </footer>
      </div>
    </div>
  </div>
  `,

  data(){
    return {
      open_modal: 'display:none',
      title: this.publication_title,
      selected_category: this.category,
      quill:null,
      categories: [],
    }
  },

  mounted(){

    this.quill = new Quill(this.$refs.QuillArea, {
      modules: {
        syntax: window.syntax,
        toolbar: window.quill_tollbar,
      },
      theme: 'snow',
      formats : window.quill_formats
    });
    this.quill.setContents(JSON.parse(this.topic));

    if(this.category) this.getCategories();
  },
  watch: {
    open_modal(val){
      if(val == 'display:none'){
        this.quill.setContents(JSON.parse(this.topic));
      }
    }
  },
  methods: {

    getCategories(){
        axios.get('/get_old_categories')
        .then(res => {
          this.categories = res.data;
        })
        .catch(err => {
          //
        });
    },

    removepublicationOrComment(id){
      if(this.publication_title && this.category){
        this.$dialog.confirm({
            title: this.$store.getters.lang.action_delete,
            message: this.$store.getters.lang.do_you_want_to_continue_this_action,
            confirmText: this.$store.getters.lang.delete,
            cancelText: this.$store.getters.lang.cancel ,
            type: 'is-danger',
            hasIcon: false,
            onConfirm: () => {
              axios.post('/remove_publication/'+id)
              .then(res => {
                if(res.data == true) {
                  this.$toast.open({
                      duration: 5000,
                      message: this.$store.getters.lang.post_has_been_deleted,
                      position: 'is-bottom',
                      type: 'is-dark'
                  });
                }else if(res.data == '401-forbidden'){
                  this.$toast.open({
                      duration: 5000,
                      message: this.$store.getters.lang.not_allowed_to_continue_this_action,
                      position: 'is-bottom',
                      type: 'is-dark'
                  });
                }
                this.$router.push('/');
              })
              .catch(err => {

              });
            }
        });
      }else{
        this.$dialog.confirm({
            title: this.$store.getters.lang.action_delete,
            message: this.$store.getters.lang.do_you_want_to_continue_this_action,
            confirmText: this.$store.getters.lang.delete,
            cancelText: this.$store.getters.lang.cancel,
            type: 'is-danger',
            hasIcon: false,
            onConfirm: () => {
              axios.post('/remove_comment/'+this.comment_id)
              .then(res => {
                if(res.data == true) {
                  this.$toast.open({
                      duration: 5000,
                      message: this.$store.getters.lang.post_has_been_deleted,
                      position: 'is-bottom',
                      type: 'is-dark'
                  });
                  document.getElementById('comment'+this.comment_id).style.display= 'none';
                }else if(res.data == '401-forbidden'){
                  this.$toast.open({
                      duration: 5000,
                      message: this.$store.getters.lang.not_allowed_to_continue_this_action,
                      position: 'is-bottom',
                      type: 'is-dark'
                  });
                }else {
                  this.$toast.open({
                      duration: 5000,
                      message: this.$store.getters.lang.error,
                      position: 'is-bottom',
                      type: 'is-dark'
                  });
                }

              })
              .catch(err => {

              });
            }
        });
      }
    },

    update(){
      this.$refs.submitBTN.className='button is-primary is-loading';
      // publication case
      if(this.publication_title && this.category){
        axios.post('/update_a_publication/'+this.$route.params.id,{
          user_id: this.$store.getters.user.id,
          title : this.title,
          category_id : this.selected_category,
          topic: JSON.stringify(this.quill.getContents()),
          topic_text : this.quill.getText()
        }).then(res => {
          if(res.status == 200) {
            this.open_modal = 'display:none';
            this.$refs.submitBTN.className='button is-primary';
            this.$emit('changed',{
              title : this.title,
              category : this.selected_category,
              topic: JSON.stringify(this.quill.getContents())
            });
            // window.location.reload();
          }
        })
        .catch(err => {
          this.$refs.submitBTN.className='button is-primary';
          this.$snackbar.open({
              message: this.$store.getters.lang.be_sure_to_fill_all_inputs,
              type: 'is-danger',
              position: 'is-bottom',
              actionText: this.$store.getters.lang.close,
              indefinite: true,
              onAction: () => {

              }
          });
        });

      // comment case
      }else{
        axios.post('/update_a_comment/'+this.comment_id,{
          user_id: this.$store.getters.user.id,
          topic: JSON.stringify(this.quill.getContents()),
          topic_text : this.quill.getText()
        }).then(res => {
          if(res.status == 200) {
            this.open_modal = 'display:none';
            this.$refs.submitBTN.className='button is-primary';
            this.$emit('changed',{
              topic: JSON.stringify(this.quill.getContents())
            });
          }
        })
        .catch(err => {
        });
      }

    }
  }

});

Vue.component('Quill-bubble-show',{
  props: ['topic'],
  template: `
    <div class="root p-0">
      <div class="p-0" ref="show"></div>
    </div>
  `,
  data(){
    return {
      quill: null
    }
  },

  watch: {
    topic:function(val){
      if(this.quill.getText().length){
        this.quill.setContents(JSON.parse(val));
      }
    }
  },

  mounted(){

    // create and bound the qill text editor to the div with ref="show"
    this.quill = new Quill(this.$refs.show, {
      theme: 'bubble',
      readOnly: true,
      modules: {
        syntax: window.syntax
      },
    });
    this.quill.setContents(JSON.parse(this.topic));

  }
});

Vue.component('Quill-Snow-Editor',{
  template: `
    <div class="root p-0">
      <div class="p-0" ref="show"></div>
    </div>
  `,

  data(){
    return {
      quill: null,
    }
  },

  mounted(){

    // create and bound the qill text editor to the div with ref="show"
    this.quill = new Quill(this.$refs.show, {
      theme: 'snow',
      readOnly: true,
      modules: {
        syntax: window.syntax,              // Include syntax module
        toolbar: window.quill_tollbar  // Include button in toolbar
      },
      formats : window.quill_formats
    });

    let interval = setInterval(()=>{
      if(this.topic) {
        this.quill.setContents(JSON.parse(this.topic));
        clearInterval(interval);
      }
    },10);
  }

});

Vue.component('reply-box',{
  props: ['comment','publicationId','publication_title'],
  template : `
  <!-- reply  textarea-->
  <div class="write-somthing" v-show="comment.open_reply == true">

    <div class="label">
      {{ $store.getters.lang.the_comment_text }}
    </div>

    <div v-if="$store.getters.user.id">
      <b-message v-if="$store.getters.user.id == undefined" type="is-success" class="column is-12 nextCaseWarning" dir="rtl">
        <a class="p-r-20" href="/register"> {{ $store.getters.lang.register_to_comment_message }} </a>
      </b-message>

      <div v-show="$store.getters.user.id">
        <div class="columns is-multiline">

          <div class="first">
            <div ref="QuillArea"></div>
          </div>
          <div class="post-comment-button">
            <button ref="saveCommentBtn" class="button is-primary" @click="sendToDB()"> {{ $store.getters.lang.comment }} </button>
          </div>

        </div>
      </div>
    </div>

    <!-- Please don't look here hahaha xD -->
    <!-- it's just copy past from the browser -->
    <div v-else>
      <div><!----> <div style=""><div class="columns is-multiline"><div class="first"><div class="ql-toolbar ql-snow"><span class="ql-formats"><span class="ql-header ql-picker"><span class="ql-picker-label"><svg viewBox="0 0 18 18"> <polygon class="ql-stroke" points="7 11 9 13 11 11 7 11"></polygon> <polygon class="ql-stroke" points="7 7 9 5 11 7 7 7"></polygon> </svg></span><span class="ql-picker-options"><span class="ql-picker-item" data-value="1"></span><span class="ql-picker-item" data-value="2"></span><span class="ql-picker-item"></span></span></span><select class="ql-header" style="display: none;"><option value="1"></option><option value="2"></option><option selected="selected"></option></select></span><span class="ql-formats"><button type="button" class="ql-bold"><svg viewBox="0 0 18 18"> <path class="ql-stroke" d="M5,4H9.5A2.5,2.5,0,0,1,12,6.5v0A2.5,2.5,0,0,1,9.5,9H5A0,0,0,0,1,5,9V4A0,0,0,0,1,5,4Z"></path> <path class="ql-stroke" d="M5,9h5.5A2.5,2.5,0,0,1,13,11.5v0A2.5,2.5,0,0,1,10.5,14H5a0,0,0,0,1,0,0V9A0,0,0,0,1,5,9Z"></path> </svg></button></span><span class="ql-formats"><button type="button" class="ql-code-block"><svg viewBox="0 0 18 18"> <polyline class="ql-even ql-stroke" points="5 7 3 9 5 11"></polyline> <polyline class="ql-even ql-stroke" points="13 7 15 9 13 11"></polyline> <line class="ql-stroke" x1="10" x2="8" y1="5" y2="13"></line> </svg></button><button type="button" class="ql-blockquote"><svg viewBox="0 0 18 18"> <rect class="ql-fill ql-stroke" height="3" width="3" x="4" y="5"></rect> <rect class="ql-fill ql-stroke" height="3" width="3" x="11" y="5"></rect> <path class="ql-even ql-fill ql-stroke" d="M7,8c0,4.031-3,5-3,5"></path> <path class="ql-even ql-fill ql-stroke" d="M14,8c0,4.031-3,5-3,5"></path> </svg></button></span><span class="ql-formats"><button type="button" class="ql-link"><svg viewBox="0 0 18 18"> <line class="ql-stroke" x1="7" x2="11" y1="7" y2="11"></line> <path class="ql-even ql-stroke" d="M8.9,4.577a3.476,3.476,0,0,1,.36,4.679A3.476,3.476,0,0,1,4.577,8.9C3.185,7.5,2.035,6.4,4.217,4.217S7.5,3.185,8.9,4.577Z"></path> <path class="ql-even ql-stroke" d="M13.423,9.1a3.476,3.476,0,0,0-4.679-.36,3.476,3.476,0,0,0,.36,4.679c1.392,1.392,2.5,2.542,4.679.36S14.815,10.5,13.423,9.1Z"></path> </svg></button></span></div><div class="ql-container ql-snow" style="
      position:  relative;
      "><div style="
        position:  absolute;
        text-align:  center;
        padding: 11px;
        background-color: #8987870a;
        /* margin: 0px; */
      ">
        <a href="/login">
          <button class="button is-primary"> {{ $store.getters.lang.login_first }} </button>
        </a>
      </div>
      <div class="ql-editor ql-blank" data-gramm="false" contenteditable="true"><p><br></p></div><div class="ql-clipboard" contenteditable="true" tabindex="-1"></div><div class="ql-tooltip ql-hidden"><a class="ql-preview" target="_blank" href="about:blank"></a><input type="text" data-formula="e=mc^2" data-link="https://quilljs.com" data-video="Embed URL"><a class="ql-action"></a><a class="ql-remove"></a></div></div></div></div></div></div>
    </div>

  </div>
  `,

  mounted(){

    this.quill = new Quill(this.$refs.QuillArea, {
      modules: {
        syntax : window.syntax,
        toolbar: window.quill_tollbar,
      },
      theme: 'snow',
      formats : window.quill_formats,
    });

  },

  data(){
    return {
      // quill
      quill: null,
      publication_id: this.publicationId,
      comments: [],
      new_reply : {
        topic: '',
        publication_id : 0,
        user_id : this.comment.user.id,
        publication_title: this.publication_title,
        publication_id: this.publicationId
      },
      window,
    }
  },

  methods:{

    // send comment to database
    sendToDB(){

      if(this.quill.getText().length < 2){
          this.$snackbar.open({
              message: this.$store.getters.lang.write_somthing,
              type: 'is-danger',
              position: 'is-bottom',
              actionText: this.$store.getters.lang.close,
              indefinite: true,
              onAction: () => {
              }
          });
          this.$refs.saveCommentBtn.className = 'button is-primary';
          return;
      }
      this.$refs.saveCommentBtn.className = 'button is-primary is-loading';
      this.new_reply.publication_title = this.publication_title;
      this.new_reply.topic = JSON.stringify(this.quill.getContents());
      this.new_reply.topic_text = this.quill.getText();
      this.new_reply.parent_comment_id = this.comment.id;
      this.new_reply.publication_id = this.publicationId;
      axios.post('/post_a_comment',this.new_reply)
      .then(res => {
        this.$emit('commingresult',{
          id: res.data,
          user: this.$store.getters.user,
          user_id: this.$store.getters.user.id,
          open_reply: false,
          topic: JSON.stringify(this.quill.getContents()),
          likes: 0,
          replies: 0,
          publication_id: this.publication_id,
          parent_comment_id: this.comment.id,
          publication_id: this.publicationId,
          publication_title: this.publication_title,
          created_at: Date.now(),
          updated_at: Date.now()
        });

        this.quill.setContents([]);
        this.$refs.saveCommentBtn.className = 'button is-primary';
        //////////////
      })
      .catch(err => {
        this.$refs.saveCommentBtn.className = 'button is-primary';
        this.$snackbar.open({
            message: this.$store.getters.lang.write_somthing,
            type: 'is-danger',
            position: 'is-bottom',
            actionText: this.$store.getters.lang.close,
            indefinite: true,
            onAction: () => {
            }
        });
      });
    },

  }
});

// Below, we are making infinit loading for comments and it's replies

// first reply
Vue.component('f-reply',{

  /*
  counter:
  -------
  The counter will be used to minmize the number of replies based on the user device.
  example: in laptop, it's normal to load 5 replies for one question, but for mobile 
  just load 4 replies. 
  

  new_comment:
  -----------
  will recieve an object contains a new reply.

  */

  props: ['commentId','publicationId','counter','publication_title','newcomment'],
  template : `
  <div ref="replies">

    <!-- loading spinner -->
    <button v-if="all_comments.length == 0" class="button chain-loading is-large is-loading"></button>

    <div v-if="all_comments.length">
    <!-- comments -->
    <div v-for="comment in all_comments" :id="'comment'+comment.id" class="comment ws-div">
      <div class="header">
        <span class="profile-photo">
          <img :src="'https://www.gravatar.com/avatar/'+comment.user.profile_img+'?d='+this.window.default_user_photo"/>
        </span>
        <router-link :to="{ name: 'user', params: { id: comment.user.id, name: comment.user.name.replace(/[\/\\\s]/g,'-') } }">
          <span class="user"> {{ comment.user.name }} </span>
        </router-link>
        <span class="time"> {{ this.window.moment.utc().fromNow(comment.user.updated_at) }} </span>
      </div>
      <p class="content">
        <Quill-bubble-show :topic="comment.topic"></Quill-bubble-show>
        <update-quill-content @changed="changecommentdata($event,comment.id)" v-if="$store.getters.user.id == comment.user.id || $store.getters.user.role == 1" :topic="comment.topic" :comment_id="comment.id"></update-quill-content>
      </p>
      <div class="footer columns">
        <div class="column">
          <span class="comments"> ({{ comment.replies }}) {{ $store.getters.lang.comment }} </span>
          <span @click="comment.open_reply = true" class="add-reply"><i class="fas fa-reply"></i>  </span>
        </div>
        <like-btn target_type="comment" :target_id="comment.id" :user_id="comment.user.id" :value="comment.likes"></like-btn>
      </div>
      <div class="replies column is-12" ref="comment">
      <!-- reply  textarea-->
      <reply-box @commingresult="addcomment($event,comment.id);comment.replies++" :publication_title="publication_title" :publicationId="publication_id" :comment="comment"></reply-box>

        <!-- /// MOBILE /// -->
        <f-reply v-if="same_counter != 0 && comment.replies > 0 && window.innerWidth <= 768" :newcomment="new_comment" :publication_title="publication_title" :counter="same_counter+1" :publicationId="publication_id" :commentId="comment.id"></f-reply>
        <!-- /// MOBILE /// -->
        <!-- show more replies -->
        <router-link v-if="same_counter == 0 && comment.replies && window.innerWidth <= 768" :id="comment.id" class="smallUnderlinedText" :to="{ name: 'fullReplies', params : { id : $route.params.id, title : publication_title.replace(/[\/\\\s]/g,'-'), commentid : comment.id } }">
         ( {{ comment.replies }} ) {{ $store.getters.lang.more_replies }}
          <i class="fas fa-caret-left"></i>
        </router-link>

        <!-- /// DESKTOP /// -->
        <f-reply v-if="same_counter != 4 && comment.replies > 0 && window.innerWidth >= 1024" :newcomment="new_comment" :publication_title="publication_title" :counter="same_counter+1" :publicationId="publication_id" :commentId="comment.id"></f-reply>
        <!-- /// DESKTOP /// -->

        <!-- show more replies -->
        <router-link v-if="same_counter == 4 && comment.replies && window.innerWidth >= 1024" :id="comment.id" class="smallUnderlinedText" :to="{ name: 'fullReplies', params : { id : $route.params.id, title : publication_title.replace(/[\/\\\s]/g,'-'), commentid : comment.id } }">
         ( {{ comment.replies }} ) {{ $store.getters.lang.more_replies }}
          <i class="fas fa-caret-left"></i>
        </router-link>

      </div>
    </div>
    </div>

    </div>
  </div>
  `,
  data(){
    return {
      moment : moment,
      new_comment: null,
      all_comments : [],
      comment_id: this.commentId,
      publication_id: this.publicationId,
      same_counter: this.counter,
      window
    }
  },

  mounted(){
    this.getComments();
  },

  methods:{

    changecommentdata(object,id){
      for (var i = 0; i < this.all_comments.length; i++) {
        if(this.all_comments[i].id == id){
          this.all_comments[i].topic = object.topic;
        }
      }
    },

    addcomment(object){
      if(object.parent_comment_id != this.commentId) this.new_comment = object;//this.getNewComment(object);
      else{
        this.all_comments.push(object);
        this.$router.push(this.$route.path+'#comment'+object.id);
      }
    },

    getNewComment(object){
      this.new_comment = object;
    },

    // get all comments
    getComments(){
      axios.get(`/comments/${this.commentId}/${this.publicationId}`)
        .then(res => {
          // make an option to open/close the reply box
          for (var i = 0; i < res.data.length; i++) {
            res.data[i].open_reply = false;
          }

          this.all_comments = res.data;
          this.comments_ready = true;
          NProgress.done();
        })
        .catch(err => {

        });
    }
  },

  watch: {
    newcomment(obj){
      if(this.commentId == obj.parent_comment_id) this.all_comments.push(obj);
      this.$router.push(this.$route.path+'#comment'+obj.id);
    }
  }
});


// component for similar publications
Vue.component('side-articles-best-of-week',{
  template: `
  <div class="side-articles">
    <div class="title">
      {{ $store.getters.lang.best_of_week }}
    </div>
    <div class="body">
      <ul>
        <li v-for="publication in publications">
        
          <text-auto-direction-rtl-ltr :title="publication.title" :id="publication.id"></text-auto-direction-rtl-ltr>

        </li>
      </ul>
    </div>
  </div>
  `,

  data(){
    return {
      publications: []
    }
  },

  mounted(){
    this.getSimilarpublications();
  },

  methods: {
      getSimilarpublications(){
        axios.get('/get_best_of_week_publications/'+this.publications.length+'/'+10)
          .then(res => {
            if(!res.data.length){
              this.$refs.ul.innerHTML = '<li class="text-center" style="border-bottom:none;"> {{ $store.getters.lang.no_results }} </li>'
            }
            this.publications = this.publications.concat(res.data);
          })
          .catch(err => {

          });
      }
  }

});

// component for publications on the same category
Vue.component('random-posts',{
  template: `
  <!-- guest user -->
  <div class="side-articles">
    <div class="title">
      {{ $store.getters.lang.we_chosen_for_you }}
    </div>
    <div class="body">
      <ul>
        <li v-for="publication in publications">
        
          <text-auto-direction-rtl-ltr :title="publication.title" :id="publication.id"></text-auto-direction-rtl-ltr>

        </li>
      </ul>
    </div>
  </div>
  `,

  data(){
    return {
      publications: []
    }
  },

  mounted(){
    this.getRandompublications();
  },

  methods: {
      getRandompublications(){
        axios.get('/get_publications_by_random')
        .then(res => {
          this.publications = res.data;
        })
        .catch(err => {

        });
      }
  }

});


Vue.component('like-btn',{
  props: ['target_type','target_id','user_id','value'],
  template: `
    <div class="column like-dislike m-b-15">
      <span ref="like_btn"> {{ formatString(get_value) }} </span>
      <i @click=" data_to_send.action = 'like';sendToDB()" class="far fa-thumbs-up m-l-5"></i>
      <i @click=" data_to_send.action = 'dislike';sendToDB()" class="far fa-thumbs-down"></i>
    </div>
  `,
  data(){
    return {
        get_value: this.value,
        data_to_send : {
          user_id : this.user_id,
          action: null,
          target_type: this.target_type,
          target_id: this.target_id
        }
    }
  },
  methods: {
    sendToDB(){
      if(!this.$store.getters.user.id){
        this.$snackbar.open({
            message: this.$store.getters.lang.login_first,
            type: 'is-warning',
            position: 'is-bottom',
            actionText: this.$store.getters.lang.close,
            indefinite: true,
            duration: 999999,
            onAction: () => {
            }
        });
        return;
      }else if(this.$store.getters.user.id == this.user_id){
        this.$snackbar.open({
            message: this.$store.getters.lang.you_cant_rate_your_posts,
            type: 'is-warning',
            position: 'is-bottom',
            actionText: this.$store.getters.lang.close,
            indefinite: true,
            onAction: () => {
            }
        })
        return;
      }

      axios.post('/like-dislike',this.data_to_send)
        .then(res => {
          if(res.data == '401-dislike'){
            // check for authorization
            if(this.data_to_send.action == 'dislike' && this.$store.getters.user.reputation < 250){
              this.$snackbar.open({
                  message: this.$store.getters.lang.not_allowed_to_continue_this_action,
                  type: 'is-warning',
                  position: 'is-bottom',
                  indefinite: true,
                  onAction: () => {
                  }
              })
            }
          }else if(res.data == '401-owner'){ /**/ }
          else{
            if(!res.data && res.data !== 0) res.data = this.$refs.like_btn.innerText;
            this.$refs.like_btn.innerText = res.data;
          }
        }).catch(err => {

        });
    },

    formatString(value){
      if(value == 0) return 0;
      if(value < 1000) return value;

      value = ''+value+'';
      let new_value = '';
      for (var i = 0; i < value.length; i++) {
        if((i%3)== 0 && i != value.length-1) new_value += value.charAt(i)+',';
        else new_value += value.charAt(i);
      }
      return new_value;
    },
  }
});

Vue.component('latest-commented-posts',{
  template: `
  <div class="side-articles">
    <div class="title">
      {{ $store.getters.lang.latest_commented_posts }}
    </div>
    <div class="body">
      <ul>
        <li v-for="item in data">
        
          <text-auto-direction-rtl-ltr :title="item.publication.title" :id="item.publication.id"></text-auto-direction-rtl-ltr>

        </li>
      </ul>
    </div>
  </div>

  `,

  mounted(){
    this.getLatestCommentedPosts();
  },

  data(){
    return {
      data: [],
    }
  },

  methods: {
    getLatestCommentedPosts(){
      axios.get('/get_latest_commented_posts/'+this.$route.params.id)
      .then(res => this.data = res.data)
      .catch(err => {
        //
      });
    }
  }
});
