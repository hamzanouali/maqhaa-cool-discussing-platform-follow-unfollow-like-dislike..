/*

Feel free to email me if you face some problems.

Email: nouali.hamza.1998@gmail.com
Author: Hamza Nouali

------------------------------------------------

Welcome here :)

Note: I am using code splitting, just take a look to webpack.mix.js

*/

import Vue from 'vue'
import VueRouter from 'vue-router/dist/vue-router.min.js'
import Buefy from 'buefy'
import 'font-awesome/css/fontawesome-all.min.css'
import 'nprogress/nprogress.css'
import NProgress from 'nprogress/nprogress.js'

/* ======= [ Heighlight.js ] ======== */
// For heighlighting code,
// if you want to activate it, just switch window.syntax = true
// and remove these lines

window.syntax = false;

/* remove this line

import 'highlight.js/styles/tomorrow.css'
var hljs = require('highlight.js')
window.hljs = hljs;

hljs.configure({   // optionally configure hljs
  // recommended for quill
  useBR: false
});

remove this line */

/* ======= [ Heighlight.js ] ======== */


/* ==== [ Quill ] ==== */

require('quill/dist/quill.snow.css');
window.Quill = require('quill/dist/quill.min.js');


/* ==== [ Quill ] ==== */

Vue.use(Buefy)

window.NProgress = NProgress;

window.Vue = Vue;

// window._ = require('lodash');

/**
 * We'll load jQuery and the Bootstrap jQuery plugin which provides support
 * for JavaScript based Bootstrap features such as modals and tabs. This
 * code may be modified to fit the specific needs of your application.
 */

// try {
//     window.$ = window.jQuery = require('jquery');
//
//     require('bootstrap-sass');
// } catch (e) {}

/**
 * We'll load the axios HTTP library which allows us to easily issue requests
 * to our Laravel back-end. This library automatically handles sending the
 * CSRF token as a header based on the value of the "XSRF" token cookie.
 */

Vue.use(VueRouter)

window.axios = require('axios/dist/axios.min.js');

window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

/**
 * Next we will register the CSRF Token as a common header with Axios so that
 * all outgoing HTTP requests automatically have it attached. This is just
 * a simple convenience so we don't have to attach every token manually.
 */

let token = document.head.querySelector('meta[name="csrf-token"]');

if (token) {
    window.axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;
} else {
    console.error('CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token');
}

/**
 * Echo exposes an expressive API for subscribing to channels and listening
 * for events that are broadcast by Laravel. Echo and event broadcasting
 * allows your team to easily build robust real-time web applications.
 */

// import Echo from 'laravel-echo'

// window.Pusher = require('pusher-js');

// window.Echo = new Echo({
//     broadcaster: 'pusher',
//     key: 'your-pusher-key'
// });
