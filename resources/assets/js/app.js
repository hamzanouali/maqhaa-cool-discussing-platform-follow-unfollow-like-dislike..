/*

Feel free to email me if you face some problems.

Email: nouali.hamza.1998@gmail.com
Author: Hamza Nouali

------------------------------------------------

Welcome here :)

I have an advice for you, please don't touche any code is written inside the beforeEach
but feel free to edit afterEach or.

Feel free to edit:
- window.quill_tollbar
- window.quill_formats
- window.default_user_photo

*/

require('./bootstrap.js');
import { store } from './store/store';
import router from './routes.js';
require('./components.js');
import routesHandler from './routes_handler';
import VueAnalytics from 'vue-analytics'

/*
 EDITOR: Quill.js
 -----------------
if you want to prevent somthing, example:

if you want to prevent posting images:
- remove it from the quill_toolbar
- remove it from quill_formats
*/

window.quill_tollbar = [
    ['bold', 'italic', 'underline'],        // toggled buttons
    ['blockquote', 'code-block'],
    [{ 'list': 'ordered'}, { 'list': 'bullet' }],         // outdent/indent
    [{ 'direction': 'rtl' }],                         // text direction
    [{ header: [1, 2, false] }],
    [{ 'align': [] }],
    ['link']
];

window.quill_formats = [
  'background',
  'bold',
  'color',
  'font',
  'code',
  'italic',
  'link',
  'size',
  'strike',
  'script',
  'underline',
  'blockquote',
  'header',
  'indent',
  'list',
  'align',
  'direction',
  'code-block',
  'formula'
  // 'image',
  // 'video',
];


/*
  What is Default user photo ?
  it's the default photo for all users before they change it from gravatar.com
*/
window.default_user_photo = 'https://i.suar.me/varoL/l';

/*
------------------------------------------------------------------------------------------
here i am catching all the 'matas' from the current route that i am trying to enter it now
and then do what should i do.
for example: if the route require authentication, i will check if i am logged in
or i will redirect myself to the login page.
------------------------------------------------------------------------------------------
*/

router.beforeEach( (to,from,next)=>{
  NProgress.start();

  // if the path does not contain the lanugage example : http://domain.com/users/000/Name
  // will be redirected to : http://domain.com/en/users/000/Name
  if(store.getters.languages.indexOf(to.path.split('/')[1]) == -1){
    return next('/'+store.getters.language+to.path);
  }

  /*
    the chosen language from the user in the path for example: http://domain.com/en/...
    is not the same as our template default language: 'ar'
  */
  if(to.params.language != store.getters.language){
    store.commit('language',to.params.language);
  }

  // 404 page handler
  if(to.matched.length == 0) {
    return next("/"+store.getters.language+"/404-page-not-found");
  }

  NProgress.set(0.6);

  // save the last route
  store.commit('last_route',to);


  let obj = new routesHandler(to,from,next);
  obj.checkForAuthentication();

  // count the not readed notification
  if(store.getters.user.id) {
    axios.get('/count-not-readed-notifications')
    .then(res => {
      store.commit('notifications',res.data);
    });
  }

} );

router.afterEach((to, from) => {
  // put the title in the browser tab
  if(to.params.title){
    document.title = to.params.title.replace(/-/g,' ');
  }
});


/*==============

GOOGLE ANALYTICS

===============*/

Vue.use(VueAnalytics, {
  id: window.google_analytics_app_key,
  router
});

new Vue({
  beforeCreate(){
    
    if(store.getters.fromState_isAuth == ''){
      axios.get('/isAuth')
      .then(res => {

        if(res.data.id) {
          store.commit('fromState_isAuth',true);
          store.commit('user',res.data);

          // count the not readed notifications
          if(store.getters.user.id) {
            axios.get('/count-not-readed-notifications')
            .then(res => {
              store.commit('notifications',res.data);
            });
          }

        }else{
          store.commit('fromState_isAuth',false);
        }
      });
    }
    
  },
  mounted(){
    // wait for the page to load then remove the fake page
    let interval = setInterval( ()=>{
      if(document.getElementById('logo') != null){
        clearInterval(interval);
        document.getElementById('FakePage').style.display='none';
      }
    },1);
    
  },
  el : '#app',
  router,
  store,
});

/**
 * Uncomment below when compiling to production
 */
// Vue.config.devtools = false;
// Vue.config.debug = false;
// Vue.config.silent = true;
