require("babel-polyfill")
import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex)

export const store = new Vuex.Store({

  // edit or call state element in strict mode (getters and mutations)
  strict : true,

  state: {

    // is this user logged-in ?

    fromState_isAuth : '',

    user: {
      name : null,
      id : null
    },
    // must login ?
    pageRequireAuth: '',

    // last browsed route (used to go back after login or sign up)
    last_route: '/',

    notifications: 0,

    // wich language we are using now in the template
    language: window.default_laguage,

    // get all languages abbreviations
    languages: Object.keys(window.dictionary),

    state_dictionary: window.dictionary,

    // dictionary contains all the languages and its constent texts and words in our template
    lang : window.dictionary[window.default_laguage],

  },

  // getters
  // called by: sotre.getters.name_of_element
  getters :{
    fromState_isAuth : state => {
      return state.fromState_isAuth;
    },
    user : state => {
      return state.user;
    },
    pageRequireAuth: state => {
      return state.pageRequireAuth;
    },
    last_route: state => {
      return state.last_route;
    },
    notifications: state => {
      return state.notifications;
    },
    language: state => {
      return state.language;
    },
    languages: state => {
      return state.languages;
    },
    lang: state => {
      return state.lang;
    },
    state_dictionary: state => {
      return state.state_dictionary;
    }
  },

  // mutations
  // called by: store.commit('name_of_mutation',values..)
  mutations: {
    fromState_isAuth: (state,bool_val) =>{
      state.fromState_isAuth = bool_val;
    },
    user: (state,obj) =>{
      state.user = obj;
    },
    pageRequireAuth : (state,bool_val) => {
      state.pageRequireAuth = bool_val;
    },
    last_route: (state,string) => {
      state.last_route = string;
    },
    notifications: (state,value) => {
      if(value > 0) document.title = '('+value+') '+ state.lang.notifications;
      state.notifications = value;
    },
    remove_notifications: state => {
      state.notifications = [];
    },
    mark_as_read: state => {
      for(let i = 0; i< state.notifications.length; i++){
        state.notifications[i].read_at = new Date();
      }
    },
    categories: (state,arr) => {
      state.categories = arr;
    },

    // set the template language
    language: (state,language) => {

      state.language = language;

      // if the chosen language does not exists in our dictionary
      if(!window.dictionary[language]){
        state.lang = window.dictionary.en;
        state.language = 'en';

      // if the chosen language exists in our dictionary
      }else {
        state.lang = window.dictionary[language];
      }

      // save the chosen language to a cookie
      document.cookie = "language="+state.language+"; expires= Infinity";

    }
  }

});
