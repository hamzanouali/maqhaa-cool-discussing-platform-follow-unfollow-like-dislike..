import VueRouter from 'vue-router/dist/vue-router.min.js'
import { store } from './store/store';

// import moment.js and set the locale language to Arabic (Algeria)
var moment = require('moment');

window.moment = moment;

// get all cookies and convert it to json
// just to get the saved language
let getCookiesAsJson = ()=>{

  let cookie_str = document.cookie;
  cookie_str = cookie_str.replace(/;/g,'","');
  cookie_str = cookie_str.replace(/=/g,'":"');
  cookie_str = cookie_str.replace(/\s/g,'');
  cookie_str = '{"'+cookie_str+'"}';
  return JSON.parse(cookie_str);

};

// in case that the template lanuage already saved in a cookie
if(getCookiesAsJson().hasOwnProperty('language')) {

  // change template language
  store.commit('language',getCookiesAsJson().language);

// else
}else{

  // save the language in a cookie
  document.cookie = "language="+store.getters.language+"; expires= Infinity";

}

// set the moment js locale
// example: 5 days ago -> english (en)
moment.locale(store.getters.language); 

function loadView(view) {
  return () => import(/* webpackChunkName: "view-[request]" */`./views/${view}.vue`)
}


let routes = [

  {
    path : '/',
    redirect : '/'+store.getters.language,
  },

  {
    path : '/:language',
    name:'home',
    beforeEnter: (to,form,next)=>{
       // check wether the passed param to :language does not exists in dictionary
       if(store.getters.state_dictionary.hasOwnProperty(to.params.language)) return next();
       else return next(store.getters.language + '/404-page-not-found');
    },
    component : loadView('home')
  },

  {
    path : '/404-page-not-found',
    name: '404page',
    beforeEnter: (to,from,next) => {
      NProgress.done();
      return next();
    },
    component : loadView('404_page')
  },

  {
    path : '/by-category/:id/:category',
    name : 'category',
    component : loadView('by_category')
  },

  {
    path: '/logout',
    beforeEnter : (to,from,next)=> {
      return window.location.href = "/logout";
    }
  },

  {
    path: '/users/:id?/:name?',
    name: 'user',
    component:  loadView('user/profile')
  },

  {
    path : '/publications/:id/:title',
    name : 'showPublication',
    component: loadView('publications/show')
  },

  {
    path : '/publications/:id/:title/full-replies/:commentid',
    name : 'fullReplies',
    component: loadView('publications/full_replies')
  },

  {
    path : '/publications/post',
    name : 'post_a_publication',
    component : loadView('publications/post'),
    meta : {
      isAuth: true,
    }
  },

  {
    path : '/categories/follow',
    name : 'followCategories',
    component : loadView('user/follow_categories'),
    meta : {
       isAuth: true,
    }
  },

  {
    path : '/notifications',
    name : 'notifications',
    component : loadView('user/notifications'),
    meta : {
       isAuth: true,
    }
  },

  {
    path : '/categories',
    name : 'categories',
    component : loadView('categories')
  },

  {
    path : '/privacy',
    name : 'inst',
    component : loadView('privacy')
  }

];

/////////////////////////////////////////////////
// to avoid '/:language' repeatation in each path
/////////////////////////////////////////////////
for (var i = 2; i < routes.length; i++) {
  routes[i].path = "/:language"+routes[i].path;
}
/////////////////////////////////////////////////

export default new VueRouter({
  // this code for jumping in the same page using #to-tag
  scrollBehavior: function(to, from, savedPosition) {
      if (to.hash) {
          NProgress.done();
          return {selector: to.hash}
      } else {
          return { x: 0, y: 0 }
      }
  },
  mode: 'history',
  routes
});
