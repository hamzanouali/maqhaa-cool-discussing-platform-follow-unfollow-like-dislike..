<?php

return [
    "join_now"=> "Join Now",
    "no_more_posts"=> "No more posts here",
    "points"=> "Points",
    "comments"=> "Comments",
    "reputation"=> "Reputation",
    "posts"=> "Posts",
    "latest_commented_posts"=> "Latest commented posts",

    "categories"=> " Categories ",
    "follow_more"=> " Follow More ",
    "more"=> "More",
    "notifications"=> " Notifications ",
    "post"=> " Post ",

    "not_allowed_to_continue_this_action"=> "You are not allowed to do this action",
    "close"=> "OK",
    "edit"=> "Edit",
    "update"=> "Update",
    "delete"=> "Delete",
    "cancel"=> "Cancel",
    "register_message"=> " This is an example text ",

    "welcome"=> "Welcome",
    "privacy"=> "Privacy",
    "contact_us"=> "Contact us",

    "title"=> "Title",
    "category"=> "Category",
    "categories"=> "Categories",
    "the_comment_text"=> "The comment",
    "text"=> "Text",

    "login"=> "Login",
    "logout"=> "Logout",

    "action_delete"=> "Delete",
    "do_you_want_to_continue_this_action"=> "Do you want to complete this action ?",
    "post_has_been_deleted"=> "Post has been deleted",

    "error"=> "Error",

    "be_sure_to_fill_all_inputs"=> "Be sur to fill all inputs",
    "comment"=> "Comment",

    "login_first"=> "Login first",
    "write_somthing"=> "Write somthing first",
    "more_replies"=> "More replies",

    "best_of_week"=> "Best of week",
    "best_of_day"=> "Best of day",
    "best_of_all"=> "Best of all",
    "common"=> "Common",
    "new"=> "New",
    "all"=> "All",
    "following"=> "Following",
    "we_chosen_for_you"=> "We chosen for you",
    "no_results"=> "No results",

    "you_cant_rate_your_posts"=> "You can\"t rate your posts ",

    "follow"=> "Follow",
    "unfollow"=> "Unfollow",
    "follow_more_categories"=> "Follow more categories",

    "page_not_found"=> "Page not found 404",
    "back_to_home_page"=> "Back to home page",

    "dashboard"=> "Dashboard",
    "home_page"=> "Home page",
    "users"=> "Users",
    "website_info"=> "Website info",
    "posting_rules"=> "Posting rules",

    "joined"=> "Joined",
    "create"=> "Create",
    "save"=> "Save",
    "show_all"=> "Show all",
    "user"=> "User",
    "admin"=> "Admin",
    "writer"=> "Writer",
    "block"=> "Block",
    "blocked"=> "Blocked",
    "role"=> "Role",
    "rates"=> "Rates",
    "username"=> "User name",
    "write_and_press_enter"=> "Write and press ENTER",
    "warning_for_deleting_categories"=> "WARNING: if you delete a category, all the posts in this category will be deleted. do not delete any category that contains posts",
	"installer" => "Installer",

	
];

?>