<?php
 return  [ 
 "reset_password_body" =>"You will be redirected to the recovering page.", 
"reset_password_header" =>"Recover password", 
"reset_password_button" =>"Reset password", 
"reset_password_regards" =>"Our regards", 
"account_activation_header" =>"Account activation", 
"account_activation_body" =>"Click on the button or on the link below in order to activate your account.", 
"account_activation_button" =>"Activate the account", 
"activate_your_account" =>"Please check you email in order to activate your account.", 
"back_to_home_page" =>"Back to home page", 
"login" =>"Login", 
"create_account" =>"Create account", 
"forgot_the_password" =>"Forgot the password", 
"email" =>"Email", 
"password" =>"Password", 
"change_password" =>"Change password", 
"update" =>"Update", 
"new_password" =>"New password", 
"confirm_password" =>"Confirm password", 
"note" =>"Note", 
"change_password_instructions" =>"If you forgot your current password,  you have to logout,     then go to login page then to forgot password", 
"edit_personal_info" =>"Edit personal info", 
"edit_profile_picture_instructions" =>"We are using Gravatar.com service for profile photos,     to change your own photo,     you have to create an account at Gravatar.com with the same email used here.", 
"recover_password" =>"Recover password", 
"send" =>"Send", 
"username" =>"User name", 
"current_password" =>"Current password", 
"blocked_account" =>"This account has been blocked.", 
"save" =>"Save", 
"translate_this" =>"Translate this", 
"saved_successfully" =>"Saved successfully.", 
"add_language" =>"Add language", 
"advenced" =>"Advenced", 
"translate_instructions" =>"How to translate: translate every thing you can to your language, example: 'Hello.' will be in spanish 'Hola.' and ':seconds' will be ':segundos'.WARNING don't remove the colon (:).", 
"language" =>"Language", 
"theme" =>"Theme", 
"installer" =>"Installer", 
"color" =>"Color", 
"style" =>"Style", 
"font" =>"Font", 
"contact_us" =>"Contact us", 
"sender" =>"Sender", 
"content" =>"Content" ]
 ;
 ?>