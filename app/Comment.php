<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
  protected $table = 'comments';
  protected $fillable = ['user_id','publication_id','parent_comment_id','replies','title','topic','likes'];

  // user that owns the comment
  public function user(){
    return $this->belongsTo('App\User');
  }

  public function like(){
    return $this->hasMany('App\Like');
  }

  public function publication(){
    return $this->belongsTo('App\Publication');
  }
}
