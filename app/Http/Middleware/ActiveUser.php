<?php

namespace App\Http\Middleware;

use Closure;

class ActiveUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(isset($_COOKIE['language'])) {
          app()->setLocale($_COOKIE['language']);
        }

        if(!\Auth::check()) return $next($request);

        if(
          \Route::current()->uri == 'logout' ||
          \Route::current()->uri == 'alert' ||
          \Route::current()->uri == 'activate-account/{token}'
        ){
          return $next($request);
        }

        if(\Auth::user()->is_active != 1){
          return redirect('/alert');
        }

        return $next($request);
    }
}
