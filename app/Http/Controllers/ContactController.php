<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ContactController extends Controller
{

  public function send(Request $request){

    $this->validate($request,[
      'email' => 'required|email',
      'content' => 'required|string|max:700'
    ]);

    \Mail::to('hamzoun220@gmail.com')->send(new \App\Mail\ContactMail($request));
    $request->session()->flash('status','تم إرسال رسالتك بنجاح');
    return view('contact');
  }

}
