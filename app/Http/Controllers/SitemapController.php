<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SitemapController extends Controller
{
    public function index()
	{
	  $publications = \DB::table('publications')->latest()->get();

	  return response()->view('sitemap.index', [
		  'publications' => $publications
	  ])->header('Content-Type', 'text/xml');
	}
}
