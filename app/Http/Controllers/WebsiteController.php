<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class WebsiteController extends Controller
{

    // change the content of certain file located in /resources/lang/language_name

    public function updateLanguage(Request $request,$language,$page){

      // get all inputs except the csrf_token
      $new_dictionary_array = $request->except(['_token']);

      // convert php array to json because
      // it's easy to convert it from  Array ( index => value ) to [ "inde" => "value" ]

      $new_dictionary_array = json_encode($new_dictionary_array,JSON_UNESCAPED_UNICODE);
      
      /*
      All these lignes will convert a json text: 
      " { "key" : "value", "key1" : { "value", } } "
      To: 
      " [ "key" => "value", "key1" => [ "value", ] ] "
      */
      $new_dictionary_array = str_replace('{'," [ \n\r ",$new_dictionary_array);
      $new_dictionary_array = str_replace('}'," ]\n\r ",$new_dictionary_array);
      $new_dictionary_array = str_replace('":','" =>' ,$new_dictionary_array);
      $new_dictionary_array = str_replace('" :','" =>',$new_dictionary_array);
      $new_dictionary_array = str_replace('",', "\", \n\r" ,$new_dictionary_array);
      $new_dictionary_array = str_replace('\\', "" ,$new_dictionary_array);
      $new_dictionary_array = str_replace('=>" [', "=> [" ,$new_dictionary_array);
      $new_dictionary_array = str_replace("]\n\r \"," ,"\n\r],\n\r" ,$new_dictionary_array);

      file_put_contents(
        base_path().'/resources/lang/'.$language.'/'.$page.'.php', 
        "<?php\n\r return ".
        sprintf($new_dictionary_array)
        .";\n\r ?>"
      );

      return redirect()->back()->with('status',__('messages.saved_successfully'));

    }

    /* 
    create new language: 
    - create new directory /resources/lang/language_abbriviation 
    - copy all files from /resources/lang/init to new directory
    */
    public function newLanguage(Request $request){
      
      $this->validate($request,[
        'language' =>  'string|max:3'
      ]);

      // to copy entire directory to another
      function copy_directory($src,$dst) {
          $dst = strtolower($dst);
          $dir = opendir($src);
          @mkdir($dst);
          while(false !== ( $file = readdir($dir)) ) {
              if (( $file != '.' ) && ( $file != '..' )) {
                  if ( is_dir($src . '/' . $file) ) {
                      recurse_copy($src . '/' . $file,$dst . '/' . $file);
                  }
                  else {
                      copy($src . '/' . $file,$dst . '/' . $file);
                  }
              }
          }
          closedir($dir);
      }

      copy_directory(
        base_path().'/resources/lang/en/',
        base_path().'/resources/lang/'.$request->language.'/'
      );

      return redirect('/installer/language');

    }
    
    // instructions means privacy, for me :)
    public function updateInstructions(Request $request)
    {
        $this->validate($request,[
          'content' => 'required|json',
        ]);
        if(\DB::table('website')->get(['instructions_of_use'])->count() == 0){
          \DB::table('website')->insert(['instructions_of_use' => $request->content]);
        }
        \DB::table('website')->update(['instructions_of_use' => $request->content]);
        return 'true';
    }


    // update posting rules 
    public function updatePostingRules(Request $request)
    {
        $this->validate($request,[
          'content' => 'required|json',
        ]);
        if(\DB::table('website')->get(['posting_rules'])->count() == 0){
          \DB::table('website')->insert(['posting_rules' => $request->content]);
        }
        \DB::table('website')->update(['posting_rules' => $request->content]);
        return 'true';
    }

}
