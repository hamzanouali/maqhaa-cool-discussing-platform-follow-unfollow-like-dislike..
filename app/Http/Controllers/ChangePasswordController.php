<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class ChangePasswordController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $this->validate($request,[
            'password' => 'required',
            'newpassword' => 'required|confirmed',
        ]);

        if(Hash::check($request->password, \Auth::user()['password'])){
            \DB::table('users')->where('id',\Auth::id())->update([
                'password' => Hash::make($request->newpassword)
            ]);
            return Hash::make($request->newpassword);
            //return redirect()->back()->with('message', 'تم تغيير كلمة السر!');
        }else{
            $errors = new \Illuminate\Support\MessageBag();

            // add your error messages:
            $errors->add('password', __('auth.wrongpassword'));

            return view('auth.passwords.changepassword')->withErrors($errors);
        }
    }

    public function updateUserInfo(Request $request){

      $this->validate($request,[
        'name' => 'required|string',
        'email' => 'required|string',
        'password' => 'required|string'
      ]);

      if(!Hash::check($request->password, \Auth::user()->password)){

        $errors = new \Illuminate\Support\MessageBag();

        // add your error messages:
        $errors->add('password', __('auth.wrongpassword'));

        return view('auth.changeinfo')
        ->with([
          'name' => \Auth::user()->name ,
          'email' => \Auth::user()->email
        ])->withErrors($errors);

      }

      \DB::table('users')
      ->where('id',\Auth::id())
      ->update([
        'name' => $request->name,
        'email' => $request->email
      ]);

      $request->session()->flash('status',__('auth.updated'));
      return redirect()->back();

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
