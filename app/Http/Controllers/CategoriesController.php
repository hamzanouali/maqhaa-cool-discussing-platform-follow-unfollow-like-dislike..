<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;

class CategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $this->validate($request,[
            'category' => 'required'
        ]);

        $obj = new Category([
            'category' => $request->category
        ]);

        $result = $obj->save();

        return json_encode($result);
    }

    public function saveForUser(Request $request)
    {
      $this->validate($request,[
        'follow_list' => 'required|json'
      ]);

      if(\DB::table('user_category')->where('user_id',\Auth::id())->get()->count()>0){
        \DB::table('user_category')->where('user_id',\Auth::id())->delete();
      }
      // return 'true';
      $request->follow_list = json_decode($request->follow_list);
      $data = [];
      for ($i=0; $i < count($request->follow_list) ; $i++) {
        array_push($data,[
          'user_id' => \Auth::id(),
          'category_id' => $request->follow_list[$i]
        ]);
      }

      \DB::table('user_category')->insert($data);
      return 'true';
    }

    public function saveOneForUser(Request $request)
    {
      $this->validate($request,[
        'category_id' => 'required|Numeric'
      ]);

      if(\DB::table('user_category')
      ->where('category_id',$request->category_id)
      ->where('user_id',\Auth::id())
      ->get()
      ->count() > 0){
        abort(401,'forbidden');
      }

      $data = [
        'user_id' => \Auth::id(),
        'category_id' => $request->category_id
      ];

      \DB::table('user_category')->insert($data);
      return 'true';
    }

    public function unfollow(Request $request)
    {
      $this->validate($request,[
        'category_id' => 'required|Numeric'
      ]);

      if(\DB::table('user_category')
      ->where('category_id',$request->category_id)
      ->where('user_id',\Auth::id())
      ->get()
      ->count() == 0){
        abort(401,'forbidden');
      }

      \DB::table('user_category')->where('user_id',\Auth::id())->where('category_id',$request->category_id)->delete();
      return 'true';
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    public function edit(Request $request)
    {
      $this->validate($request, [
        'name' => 'required|string',
        'category_id' => 'required|numeric'
      ]);

      \DB::table('categories')->where('id',$request->category_id)->update([ 'category' => $request->name ]);
      return 'true';
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $this->validate($request,[
            'category_id' => 'required|numeric'
        ]);

        \DB::table('user_category')->where('category_id',$request->category_id)->delete();
        \DB::table('publications')->where('category_id',$request->category_id)->delete();
        \DB::table('categories')->whereId($request->category_id)->delete();
    }
}
