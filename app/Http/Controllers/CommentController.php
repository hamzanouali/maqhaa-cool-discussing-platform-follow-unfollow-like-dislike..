<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Comment;
use App\Publication;
use App\Reply;


class CommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $this->validate($request,[
          'publication_id' => 'required|integer',
          'parent_comment_id' => 'required|integer',
          'topic' => 'required|string',
          'topic_text' => 'required|string',
          'publication_title' => 'required|string',
          'user_id' => 'required|integer'
        ]);

        \DB::beginTransaction();

        // checking whether the publication is not closed
        if(//////
          count(Publication::where('id','=',$request->publication_id)->get(['is_closed'])) > 0
          &&
          Publication::where('id','=',$request->publication_id)->get(['is_closed'])[0]->is_closed == 1)
        {////////
          \DB::rollBack();
          return false;
        }////////

        // contains null now
        $obj;
        // checking whether the comment that we want to reply on it is not closed
        $comm = Comment::where('id','=',$request->parent_comment_id);
        if(count($comm->get(['is_closed'])) > 0 && $comm->get(['is_closed'])[0]->is_closed == 1){
          \DB::rollBack();
          return false;
        }

        // increment replies for that comment
        Comment::where('id','=',$request->parent_comment_id)->increment('replies',1);

        // increment comments number in publication with that id
        Publication::whereId($request->publication_id)->increment('comments',1);

        $id = \Auth::id();
        if(\Auth::user()['role'] == 2){
            $id = \DB::table('users')->where('role',2)->inRandomOrder()->first()->id;
        }

        // savine data
        $obj = new Comment([
          'user_id' => $id,
          'publication_id' => $request->publication_id,
          'parent_comment_id' => $request->parent_comment_id,
          'topic' => $request->topic
        ]);
        \DB::table('user_info')->where('user_id',\Auth::id())->increment('comments');
        $res = $obj->save();

        // don't notify the owner
        if($request->user_id != \Auth::id()){
          $user = \App\User::find($request->user_id);
          $user->notify(new \App\Notifications\publicationNotification($request->publication_id,\Auth::user()['name'],$request->publication_title));
        }

        \DB::commit();
        return json_encode($obj->id);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
          'user_id' => 'required|Numeric',
          'topic' => 'required|json',
          'topic_text' => 'required|string'
        ]);

        if($request->user_id != \Auth::id() && \Auth::user()['role'] == 0) return false;

        \DB::table('comments')->where('id',$id)->update([
          'topic' => $request->topic
        ]);

        return 'true';
    }
}
