<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Like extends Model
{
    protected $fillable = ['user_id','target_id','action','target_type'];

    public function user(){
      return $this->belongsTo('App\User');
    }

    public function publication(){
      return $this->belongsTo('App\Publication');
    }

    public function comment(){
      return $this->belongsTo('App\Comment');
    }
}
